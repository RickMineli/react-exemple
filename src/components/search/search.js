import React, { Component } from 'react'
import { Form, InputGroup, Col, Row, Button, Card, ListGroup } from 'react-bootstrap'

// import Calendar from "../../components/calendar/calendar"
// import Drawer from 'react-drag-drawer'

/* COLOCAR OS ICONES */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt, faCalendarDay } from '@fortawesome/free-solid-svg-icons'
import { isMobile } from 'react-device-detect'

// import moment from 'moment'

import './search.css'

// --------------------------- To date
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import 'react-daterange-picker/dist/css/react-calendar.css' // For some basic styling. (OPTIONAL)
import CustomInputDate from '../../components/dateComponent'

import { Animated } from 'react-animated-css'

export default class search extends Component {
  render () {
    const { showSearchComponent,
      loading,
      locationInput,
      dateInput,
      formValid,
      hideDescription,
      heightChange,
      isHeightChange,
      onSearch,
      handleKeyPress,
      handleLocationInput,
      onCalendarChange

    } = this.props
    return (
    // <div>

      <div className='backgroundImage' style={{ height: '' + heightChange + 'px' }}>
        <Animated animationOut='fadeOutUp' isVisible={showSearchComponent} animationOutDelay={1000} >

          {isHeightChange !== true && (

            <Animated animationOut='fadeOutUp' isVisible={hideDescription} animationOutDelay={1000} >

              <div className='formPosition'>
                <Row>
                  <Card.Title className='cardTitleFeed'>Find the best place to meet and discover</Card.Title>
                  {/* <h1 className='coverTitles'>Find the best place to meet and discover</h1> */}
                </Row>
                <ListGroup.Item className='line' />
                <Row>
                  <Card.Subtitle className='cardSubtitleFeed'>For some reason, there's a place waiting for you!</Card.Subtitle>
                  {/* <h2 className='coverSubtitles'>For some reason, there's a place waiting for you!</h2> */}
                </Row>
              </div>
            </Animated>
          )}
          <div className='alingInpBtnFeed'>
            <div className='formPosition'>
              <Row>
                <div className='formPositionLocation'>
                  <Col>
                    <InputGroup className='locationForm'>
                      <InputGroup.Prepend>
                        <InputGroup.Text id='inputGroupPrepend'><FontAwesomeIcon className='iconFeedColor' icon={faMapMarkerAlt} /></InputGroup.Text>
                      </InputGroup.Prepend>
                      <Form.Control
                        type='text'
                        placeholder='Location'
                        id='locationInput' name='locationInput'
                        // isInvalid={this.state.formErrors.maxParticipants}
                        value={locationInput}
                        onChange={handleLocationInput}
                        onKeyPress={handleKeyPress}
                      />
                    </InputGroup>
                  </Col>
                </div>
                <div className='formPositionDate'>
                  <Col>
                    <InputGroup.Text className='inputGorupDateFeed'>
                      <div className='divIconDateFeed'>
                        <FontAwesomeIcon
                          className='iconFeedColor'
                          icon={faCalendarDay}
                        />
                      </div>

                      {isMobile && (
                        <DatePicker
                          value={dateInput}
                          selected={dateInput}
                          onSelect={onCalendarChange} // when day is clicked
                          customInput={<CustomInputDate className='dateSerch' />}
                          // showTimeInput={true}
                          showMonthDropdown
                          showYearDropdown
                          dateFormat='MMMM d, yyyy'
                          withPortal
                          minDate={(new Date())}
                        />
                      )}
                      {!isMobile && (
                        <DatePicker
                          value={dateInput}
                          selected={dateInput}
                          onSelect={onCalendarChange} // when day is clicked
                          customInput={<CustomInputDate className='dateSerch' />}
                          // showTimeInput={true}
                          showMonthDropdown
                          showYearDropdown
                          dateFormat='MMMM d, yyyy'
                          // withPortal
                          minDate={(new Date())}
                        />
                      )}

                    </InputGroup.Text>

                  </Col>
                </div>
              </Row>
            </div>
            <div className='buttonsFeedPosition'>
              <Row>
                <div className='buttonPositionSearch'>
                  <Col >
                    <Button className='searchButton'
                      disabled={!formValid || loading}
                      // onClick={() => this.buttonSearchPress()
                      onClick={() => onSearch()}

                    >
                      {loading ? 'Searching...' : 'Search'}
                    </Button>
                  </Col>
                </div>
                {/* <div className='buttonPositionSugestion'>
                  <Col>
                    <Button className='sugestionButton'>Surprise me!</Button>
                  </Col>
                </div> */}
              </Row>
            </div>
          </div>
        </Animated>

      </div>

    )
  }
}
