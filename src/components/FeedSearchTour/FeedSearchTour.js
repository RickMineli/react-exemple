import React, { Component } from 'react'
import { Row, Col, Container, Card, ListGroup, Pagination, Button } from 'react-bootstrap'
// import Zoom from 'react-reveal/Zoom'
/* COLOCAR OS ICONES */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import './FeedSearchTour.css'

export default class FeedSearchTour extends Component {
  render() {
    const { tours, changePage, myTours, removeTour } = this.props
    return (
      <div >
        {/* <Zoom> */}
        <div>
          {tours && (
            <div>
              {tours.content.map((tour) => (
                <div key={tour.id}>
                  <ListGroup.Item action href={`/tour/${tour.id}`} style={{ position: 'inherit' }}>
                    <Container className='containerFeedSearch'>
                      <Row >
                        <Card.Img className='tourImageSearch' variant='top' src={tour.mainImage.link} />
                        <Col style={{ position: 'inherit' }}>
                          <Card.Body className='TourBodySearch'>
                            <Card.Title className='TourTitle'>{tour.region}</Card.Title>
                            <Card.Subtitle>{tour.title}</Card.Subtitle>
                            <Card.Text className='subtittleFeedSearch'>
                              {tour.description}
                            </Card.Text>
                            <Card.Text className='TourPrice'>Price: ${tour.price} | Estimated Hours: {tour.estimatedHour}</Card.Text>
                          </Card.Body>

                        </Col>
                      </Row>
                    </Container>
                  </ListGroup.Item>
                </div>
              ))}

              <Pagination className='paginationPosition'>
                {tours.number === 0 && (
                  <Pagination>
                    <Pagination.Item active>{tours.number + 1}</Pagination.Item>
                    {tours.totalPages > 1 && (
                      <Pagination.Item onClick={() => changePage(tours.number + 1)}>{tours.number + 2}</Pagination.Item>
                    )}
                    {tours.totalPages > 2 && (
                      <Pagination.Item onClick={() => changePage(tours.number + 2)}>{tours.number + 3}</Pagination.Item>
                    )}
                  </Pagination>
                )}
                {tours.number > 1 && (
                  <Pagination>
                    <Pagination.Item onClick={() => changePage(0)}>{1}</Pagination.Item>
                  </Pagination>
                )}
                {tours.number > 2 && (
                  <Pagination>
                    <Pagination.Prev onClick={() => changePage(tours.number - 2)} />
                  </Pagination>
                )}
                {tours.totalPages > 1 && tours.number >= 1 && (
                  <Pagination>
                    <Pagination.Item onClick={() => changePage(tours.number - 1)}>{tours.number}</Pagination.Item>
                  </Pagination>
                )}
                {tours.number >= 1 && (
                  <Pagination>
                    <Pagination.Item active>{tours.number + 1}</Pagination.Item>
                    {tours.totalPages > tours.number + 1 && (
                      <Pagination.Item onClick={() => changePage(tours.number + 1)}>{tours.number + 2}</Pagination.Item>
                    )}
                    {tours.totalPages > tours.number + 2 && (
                      <Pagination.Item onClick={() => changePage(tours.number + 2)} >{tours.number + 3}</Pagination.Item>
                    )}
                  </Pagination>
                )}
              </Pagination>
            </div>
          )}


          {myTours && (
            <div>
              {myTours.map((tour) => (
                <div key={tour.id}>
                  <ListGroup.Item style={{ position: 'inherit' }}>
                    <Container className='containerFeedSearch'>
                      <Row >
                        <Card.Img className='tourImageSearch' variant='top' src={tour.mainImage.link} />
                        <Col style={{ position: 'inherit' }}>
                          <Card.Body className='TourBodySearch'>
                            <Card.Title className='TourTitle'>{tour.region}</Card.Title>
                            <Card.Subtitle>{tour.title}</Card.Subtitle>
                            <Card.Text className='subtittleFeedSearch'>
                              {tour.description}
                            </Card.Text>
                            <Card.Text className='TourPrice'>Price: ${tour.price} | Estimated Hours: {tour.estimatedHour}</Card.Text>
                          </Card.Body>
                          <div className='alingButtonEditRemoveFeedSearch'>
                            <Button href={`/myTourRegister/${tour.id}`} variant="link" className='buttonEditFeedSearch'>
                              <FontAwesomeIcon className='iconColor2' icon={faPencilAlt} />
                            </Button>
                            <Button onClick={() => removeTour(tour)} variant="link" className='buttonRemoveFeedSearch'>
                              <FontAwesomeIcon className='iconHeart' icon={faTrashAlt} />
                            </Button>

                          </div>
                        </Col>
                      </Row>
                    </Container>
                  </ListGroup.Item>
                </div>
              ))}
            </div>
          )}


        </div>
        {/* </Zoom> */}
      </div>
    )
  }
}
