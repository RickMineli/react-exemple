import React, { Component } from 'react'
import { Card, Col, Row, ListGroup } from 'react-bootstrap'
import { Icon } from 'react-icons-kit'
import { arrowGraphUpRight } from 'react-icons-kit/ionicons/arrowGraphUpRight'
import { androidFavorite } from 'react-icons-kit/ionicons/androidFavorite'
import loadingImg from '../../imgs/loadingGifs/loading12.gif'
// import image2 from '../../imgs/cityImages/SydneyCity01.jpg'
// import image3 from '../../imgs/mountain3.jpg'
import './profile.css'
import * as serviceTours from '../../services/tours.service'

export default class Profile extends Component {
  constructor(props) {
    super(props)

    this.state = {
      userId: this.props.userId,
      listOfTours: [],
      nameGuide: '',

      guideImage: loadingImg,
      guideDescription: '...'
    }
  }
  async componentDidMount() {
    try {
      if (this.state.userId) {
        serviceTours.getGuideTours(this.state.userId)
          .then(res => {
            let data = res.data
            if (data.length > 0) {
              console.log(data)
              this.setState({
                loading: false,
                listOfTours: data,
                nameGuide: data[0].guide.name,
                cityGuide: data[0].guide.city,
                stateGuide: data[0].guide.state,
                countryGuide: data[0].guide.country,
                guideDescription: data[0].guide.description,
                guideImage: data[0].guide.picture.link
              })
            }
          }, err => {
            this.handleError('unabledataget')
            console.log(err)
          })
      } else {
        this.creteNewDate()
      }
    } catch (error) {
      this.handleError('unabledataget')
      console.log(error)
    }
  }

  render() {
    return (
      <div >
        <div >
          <Col className='colDetails'>
            <Row>
              <Card.Img className='profilePic' src={this.state.guideImage} />
              <Card className='colGuideDetais'>
                <Card.Title className='cardNameGuide'>{this.state.nameGuide}</Card.Title>
                <Card.Subtitle className='cardGuideInf'>
                  <Icon className='divFavorite' size={32} icon={androidFavorite} /> 5.0
                  <Icon className='divTrips' size={32} icon={arrowGraphUpRight} /> {this.state.listOfTours.length} Trips
                </Card.Subtitle>
                <Card.Text className='cardDescriptionGuide'>
                  {this.state.guideDescription}
                </Card.Text>
              </Card>
            </Row>
          </Col>
          <Card.Title className='newToursTitleProfile'>New {this.state.nameGuide} Tours</Card.Title>
          <div className='divTripsProfile'>
            {this.state.listOfTours.map((tour) => (
              <div key={tour.id} className='divTourProfile'>
                <ListGroup.Item action href={`/tour/${tour.id}`} className='listGroupItemProfile'>
                  <Card className='cardProfile'>
                    <Card.Img className='imgToursProfile' src={tour.mainImage.link} />
                    <Card.ImgOverlay className='carImgdOverlayProfile'>
                      <Card.Title className='cardTitleTourProfile'>{tour.title}</Card.Title>
                      <Card.Text className='descTourProfile'>{tour.description}</Card.Text>
                    </Card.ImgOverlay>
                  </Card>
                </ListGroup.Item>
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
