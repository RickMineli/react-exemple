import React, { Component } from 'react'
import { Col, Row, Card } from 'react-bootstrap'
import Lightbox from 'react-image-lightbox'
import 'react-image-lightbox/style.css'
/* COLOCAR OS ICONES */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGlobeAmericas, faHeart } from '@fortawesome/free-solid-svg-icons'

import './cover.css'

export default class Cover extends Component {
  constructor(props) {
    super(props)

    this.state = {
      photoIndex: 0,
      isOpen: false
    }
  }

  render() {
    const { photoIndex, isOpen } = this.state
    const { mainImage, imagesLink, title } = this.props
    return (
      <div >
        {isOpen && (
          <Lightbox
            mainSrc={imagesLink[photoIndex]}
            nextSrc={imagesLink[(photoIndex + 1) % imagesLink.length]}
            prevSrc={imagesLink[(photoIndex + imagesLink.length - 1) % imagesLink.length]}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + imagesLink.length - 1) % imagesLink.length
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % imagesLink.length
              })
            }
          />
        )}
        <div className='backgroundCover' style={{ backgroundImage: 'url(' + mainImage.link + ')' }} onClick={() => this.setState({ isOpen: true, photoIndex: imagesLink.length - 1 })}>
          <Card.Title className='coverTitle'>{title}</Card.Title>
          <div className='iconsPosition'>
            <Row className='rowIconPosition'>
              <div>
                <Col>
                  <Card.Subtitle className='subtitleCover'> <FontAwesomeIcon className='iconCoverColor' icon={faGlobeAmericas} />TRENDING  </Card.Subtitle>
                </Col>
              </div>
              <div>
                <Col >
                  <Card.Subtitle className='subtitleCover'> <FontAwesomeIcon className='iconCoverColor' icon={faHeart} /> 5.0 RATING</Card.Subtitle>
                </Col>
              </div>
            </Row>
          </div>
        </div>
      </div >
    )
  }
}