import React, { Component } from 'react'
import { Card, CardDeck, Row } from 'react-bootstrap'
import Lightbox from 'react-image-lightbox'
import 'react-image-lightbox/style.css'

import './description.css'
// import Imagem1 from '../../../imgs/teste1.jpg'
// import Imagem2 from '../../../imgs/creteboat.jpg'
// import Imagem3 from '../../../imgs/landscape.jpg'
// import Imagem4 from '../../../imgs/everest.jpg'
// import Imagem5 from '../../../imgs/escalada.jpg'


export default class Description extends Component {
  constructor(props) {
    super(props)

    this.state = {
      photoIndex: 0,
      isOpen: false
    }
  }
  render() {
    const { photoIndex, isOpen } = this.state
    const { description, otherImages, imagesLink, region } = this.props
    // const images = [
    //   Imagem1, Imagem2, Imagem3, Imagem4, Imagem5
    // ]

    return (
      <div >

        {isOpen && (
          <Lightbox
            mainSrc={imagesLink[photoIndex]}
            nextSrc={imagesLink[(photoIndex + 1) % imagesLink.length]}
            prevSrc={imagesLink[(photoIndex + imagesLink.length - 1) % imagesLink.length]}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + imagesLink.length - 1) % imagesLink.length
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % imagesLink.length
              })
            }
          />
        )}

        <Card.Title className='descriptionPostTitle'  >Description</Card.Title>
        <div className='TamanhoColuna'>
          <Card.Subtitle>Region: {region}</Card.Subtitle>
        </div>
        <div className='TamanhoColuna'>
          <Card.Text className='descriptionPostText' >{description}</Card.Text>
        </div>
        {/* <div className='rowNewImg'> */}
          <CardDeck className='cardDeckDescription'>
            {otherImages.map((image, index) => (
              <Row key={image.id} className='rowNewImg'>
                <Card className='cardImgDescription' >
                  {image.fireBaseId !== 'null' && (
                    <Card.Img className='cardImgPostDescription' src={image.link} onClick={() => this.setState({ isOpen: true, photoIndex: index })} />
                  )}
                </Card>
              </Row>
            ))}
          </CardDeck>
        {/* </div> */}
      </div>
    )
  }
}
