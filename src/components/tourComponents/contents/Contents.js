import React, { Component } from 'react'
import { ListGroup, Card, Row, Form, Image, Button, Col } from 'react-bootstrap'

import './contents.css'
// import Beach from '../../../imgs/beachs.jpg'
import True from '../../../imgs/Icons/TrueIcon.png'
import False from '../../../imgs/Icons/FalseIcon.png'
import moment from 'moment'

export default class Content extends Component {
  constructor(props) {
    super(props);

    this.state = {
      //----------------- Validations
      formValid: false,
      checkCondition: false,
      checkDate: false,
      formErrors: {
        condition: '',
        date: ''
      },

    };
  }

  handleCheckBox = () => {
    let fieldValidationErrors = this.state.formErrors;

    if (this.state.checkCondition === false) {
      fieldValidationErrors.condition = false
      this.setState({ checkCondition: true, formErrors: fieldValidationErrors }, this.validateForm)
    } else {
      fieldValidationErrors.condition = true
      this.setState({ checkCondition: false, formErrors: fieldValidationErrors }, this.validateForm)
    }
  }

  handleCheckBoxDate(value, index, tourDate) {
    let fieldValidationErrors = this.state.formErrors;
    fieldValidationErrors.date = false
    this.props.dateChosen(tourDate)
    this.setState({
      checkDate: true
    }, this.validateForm)
  }

  validateForm() {
    this.setState({
      formValid:
        this.state.formErrors.condition === false &&
        this.state.formErrors.date === false
    });
  }

  render() {
    const { guide, showBooking, guidePicture, allData, onSchedule } = this.props
    return (
      <div>
        <ListGroup className='listGroupContent' variant="flush">

          <ListGroup.Item className='listGroupItemContent'>
            <a className='guideNames' href={`/profile/${allData.guide.id}`} >
              <Card.Text> <Image className='avatarContentPost' src={guidePicture} /> {guide.name}</Card.Text>
            </a>
            <Row>

            </Row>
            <Card.Text>Conhecedor das mais diversas paisagens e sports outdoor do mundo!</Card.Text>
          </ListGroup.Item>
          <ListGroup.Item className='listGroupItemContent'>
            <Card.Text className='textPrice'>Price per person ${allData.price}</Card.Text>
            <Card.Text className='textEstimatedHour'>Estimated time: {allData.estimatedHour}</Card.Text>
            {/* </ListGroup.Item> */}
            {/* <ListGroup.Item className='listGroupItemContent'> */}
            {allData.tourDates.map((tourDate, index) => (
              <div className='divDates' key={index}>
                <ListGroup>
                  {new Date(tourDate) < new Date(allData.realCurrentTime) && (
                    <ListGroup.Item className='listGroupItemContentDateDisabled' action disabled>
                      <Card.Text className='iconeTrueFalse'><Image src={False} /> {moment(Date.parse(tourDate)).format("MMM DD YYYY - HH:MM")}</Card.Text>
                      {/* <Card.Text className='iconeTrueFalse'>{moment(Date.parse(tourDate)).format("MMM DD YYYY - HH:MM")}</Card.Text> */}
                    </ListGroup.Item>

                  )}


                  {new Date(tourDate) >= new Date(allData.realCurrentTime) && (
                    <div>
                      <fieldset>
                        <Form.Group>
                          <Col >
                            <Form.Check.Input
                              className='checkEnabled'
                              type="radio"
                              name="formHorizontalRadios"
                              id="formHorizontalRadios1"
                              onClick={(value) => this.handleCheckBoxDate(value, index, tourDate)}
                            />
                            <Form.Check.Label>{`${moment(Date.parse(tourDate)).format("MMM DD YYYY - HH:MM")}`}</Form.Check.Label>
                          </Col>
                        </Form.Group>
                      </fieldset>
                    </div>
                  )}
                </ListGroup>
              </div>
            ))}
            {allData.stay === true && (
              <Card.Text className='iconeTrueFalse'><Image src={True} />This Price include Stay!</Card.Text>
            )}
            {allData.stay === false && (
              <Card.Text className='iconeTrueFalse'><Image src={False} />This Price does not include Stay!</Card.Text>
            )}
            {allData.transport === true && (
              <Card.Text className='iconeTrueFalse'><Image src={True} />This Price includes Transport!</Card.Text>
            )}
            {allData.transport === false && (
              <Card.Text className='iconeTrueFalse'><Image src={False} />This Price does not includes Transport!</Card.Text>
            )}
          </ListGroup.Item>
          {!showBooking &&
            <ListGroup.Item className='listGroupItemContent'>
              <Form.Check className='iconeTrueFalse' type="checkbox" label="I Agree with the Terms and Conditons!" onClick={this.handleCheckBox} />
              {/* </ListGroup.Item> */}
              {/* <ListGroup.Item className='listGroupItemContent' style={{ paddingLeft: '10px', paddingRight: '5px' }}> */}
              <Button className='buttonScheduleContentPost' variant='primary'
                disabled={!this.state.formValid}
                onClick={() => onSchedule(this.state.checkDate)}>
                Schedule
              </Button>
            </ListGroup.Item>
          }

        </ListGroup>
      </div>
    )
  }
}