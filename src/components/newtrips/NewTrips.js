import React, { Component } from 'react'
// import { Row, Col, Container } from 'react-bootstrap'
import { Card, Row, Col, CardDeck, Button } from 'react-bootstrap'
import { faClock } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import moment from 'moment'


// import image1 from '../../imgs/ny.jpg'
// import image2 from '../../imgs/cityImages/WashingtonCity.png'
// import image3 from '../../imgs/museum.jpg'
// import image4 from '../../imgs/louvre.jpg'
// import image5 from '../../imgs/cityImages/SydneyCity01.jpg'


import './newtrips.css'

export default class NewTrips extends Component {
  render() {
    const { newTripsToursBig, newTripsToursLittle } = this.props
    return (
      <div className='backgroundNewTrips'>
        <div className='divPositionsNewTrips'>
          <Row>
            <Card.Title className="titleNewTrips">New Trips</Card.Title>
          </Row>
        </div>
        <div className='divNewTrips'>
          <div className='divPositionsNewTrips'>

            <Row className='rowImgNewTripsTop'>

              <div className='colLeftNewTrips'>
                {newTripsToursBig.map((tour, index) => (
                  <div key={index} className='divLeftNewTrips'>
                    <Card >
                      <Button href={`/tour/${tour.id}`} variant="link" className='buttonLinkNewTrips'>
                        <Card.Img className='cardImgNewTrip' variant="bottom" src={tour.mainImage.link} />
                        <Card.ImgOverlay className='imgOverlayNewTrips'>
                          <div className='divBodyNewTrip'>
                            <Card.Title className='cardTitleNewTrip'>{tour.title}</Card.Title>
                            <Card.Text className='cardSubtitleNewTrip'>{tour.description}</Card.Text>
                          </div>
                        </Card.ImgOverlay>
                      </Button>
                    </Card>
                  </div>
                ))}
              </div>
              <Col>
                <div className='colRighttNewTrips'>
                  <CardDeck>
                    {newTripsToursLittle.map((tour, index) => (
                      <div key={index} className='divRightNewTrips'>
                        <Card border='light' style={{ position: 'inherit', background: '#F7F6F6' }} className="text-dark">
                          <Button href={`/tour/${tour.id}`} variant="link" className='buttonLinkNewTrips'>
                            <Card.Img className='cardImgNewTripsLittle' variant="top" src={tour.mainImage.link} />
                          </Button>
                          <Card.Body className='bodyNewTrips'>
                            <Card.Title className='rightTripTitle'>{tour.title}</Card.Title>
                            <Row>
                              <Card.Text className='rigthTripDescription'>
                                <FontAwesomeIcon icon={faClock} /> {`${moment(Date.parse(new Date(tour.creationDate))).format("MMM DD YYYY")}`}
                              </Card.Text>
                              <a className='rigthTripAuthor' href={`/profile/${tour.guide.id}`}>
                                {tour.guide.name}
                              </a>
                            </Row>
                          </Card.Body>
                        </Card>
                      </div>
                    ))}
                  </CardDeck>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div >
    )
  }
}
