import React, { Component } from 'react'
import Cards from 'react-credit-cards';
import { Button, Card, Form } from 'react-bootstrap'

import {
    formatCreditCardNumber,
    formatCVC,
    formatExpirationDate,
    formatFormData,
} from './utils';
import './styles.css';

import 'react-credit-cards/es/styles-compiled.css';

export default class CreditCards extends Component {

    constructor(props) {
        super(props);

        this.state = {
            number: '',
            name: '',
            expiry: '',
            cvc: '',
            issuer: '',
            focused: '',
            formData: null,

            dataCreditCard: {
                number: '',
                name: '',
                expiry: '',
                cvc: '',
                issuer: '',
                focused: '',
                formData: null,
            },


            //----------------- Validations
            formValidCreditCard: false,
            formErrorsCreditCard: [{
                number: '',
                name: '',
                expiry: '',
                cvc: '',
            }],
            formCheckCreditCard: [{
                number: '',
                name: '',
                expiry: '',
                cvc: '',
            }]
        }


    }


    handleCallback = ({ issuer }, isValid) => {
        if (isValid) {
            this.setState({ issuer });
        }
    };

    handleInputFocus = ({ target }) => {
        this.setState({
            focused: target.name,
        });
    };

    handleInputChange = ({ target }) => {
        const { name, value } = target;

        let fieldValidationErrors = this.state.formErrorsCreditCard;
        let fieldValidationCheck = this.state.formCheckCreditCard;

        if (target.name === 'number') {
            target.value = formatCreditCardNumber(target.value);
        } else if (target.name === 'expiry') {
            target.value = formatExpirationDate(target.value);
        } else if (target.name === 'cvc') {
            target.value = formatCVC(target.value);
        }

        let fieldDataCreditCard = this.state.dataCreditCard
        switch (name) {
            case 'number':
                if (value.length <= 22) {
                    let numberValid = value.length >= 19 && value.length <= 22;
                    fieldValidationErrors.number = numberValid ? false : true;
                    fieldValidationCheck.number = numberValid ? true : false;
                    fieldDataCreditCard.number = value
                }
                break;
            case 'name':
                if (value.length <= 50) {
                    let nameValid = value.length >= 7 && value.length <= 50;
                    fieldValidationErrors.name = nameValid ? false : true;
                    fieldValidationCheck.name = nameValid ? true : false;
                    fieldDataCreditCard.name = value
                }
                break;
            case 'expiry':
                if (value.length <= 5) {
                    // let testDate = new Date(value)
                    let getJustYear = 0
                    getJustYear = JSON.parse("20" + value.slice(0, 2))
                    let getJustMonth = value.slice(3, 5)
                    let validMonth = false
                    let validYear = false
                    if (getJustYear >= new Date().getFullYear()) {
                        validYear = true
                        if (getJustMonth > 0 && getJustMonth <= 12) {
                            if (getJustYear === new Date().getFullYear()) {
                                if (getJustMonth >= new Date().getMonth() + 1) {
                                    validMonth = true
                                }
                            } else {
                                validMonth = true
                            }
                        }
                    }

                    let expiryValid = value.length === 5 && validYear === true && validMonth === true;
                    fieldValidationErrors.expiry = expiryValid ? false : true;
                    fieldValidationCheck.expiry = expiryValid ? true : false;
                    fieldDataCreditCard.expiry = value
                    console.log(expiryValid)
                }
                break;
            case 'cvc':
                if (value.length <= 4) {
                    let cvcValid = value.length >= 3 && value.length <= 4;
                    fieldValidationErrors.cvc = cvcValid ? false : true;
                    fieldValidationCheck.cvc = cvcValid ? true : false;
                    fieldDataCreditCard.cvc = value
                }
                break;
            default:
                break;
        }

        this.setState({
            [target.name]: target.value,
            dataCreditCard: fieldDataCreditCard,
            formErrorsCreditCard: fieldValidationErrors,
            formCheckCreditCard: fieldValidationCheck
        }, () => this.validateForm());
    }

    validateForm() {
        this.setState({
            formValidCreditCard:
                this.state.formErrorsCreditCard.number === false &&
                this.state.formErrorsCreditCard.name === false &&
                this.state.formErrorsCreditCard.expiry === false &&
                this.state.formErrorsCreditCard.cvc === false
        });
    }



    handleSubmit = e => {
        e.preventDefault();
        // const { issuer } = this.state;
        const formData = [...e.target.elements]
            .filter(d => d.name)
            .reduce((acc, d) => {
                acc[d.name] = d.value;
                return acc;
            }, {});

        this.setState({ formData });
        this.form.reset();
    };

    render() {
        const { name, number, expiry, cvc, focused, issuer, formData, dataCreditCard } = this.state;
        const { loading, payButton, price, participants, formValid } = this.props;
        return (
            <Card>

                <div key="Payment">
                    <div className="App-payment">
                        <Cards
                            number={number}
                            name={name}
                            expiry={expiry}
                            cvc={cvc}
                            focused={focused}
                            callback={this.handleCallback}
                        />

                        <form ref={c => (this.form = c)} onSubmit={this.handleSubmit}>
                            {/* <Col className='colCreditCard'> */}
                            <Form.Row>
                                <Form.Label className="labelCreditCardTotalPrice">Total Participants: {participants.length}</Form.Label>
                            </Form.Row>
                            <Form.Row>
                                <Form.Label className="labelCreditCardTotalPrice">Total Price: ${price * participants.length}</Form.Label>
                            </Form.Row>
                            {/* </Col> */}
                            <div className="form-group">
                                <input
                                    type="tel"
                                    name="number"
                                    className="form-control"
                                    placeholder="Card Number"
                                    pattern="[\d| ]{16,22}"
                                    required
                                    onChange={this.handleInputChange}
                                    onFocus={this.handleInputFocus}
                                />
                                <small>E.g.: 49..., 51..., 36..., 37...</small>
                                {this.state.formErrorsCreditCard.number && this.state.dataCreditCard.number.length < 19 &&
                                    <div className="help-blockRegUser">Complete Card Number is required</div>
                                }
                            </div>
                            <div className="form-group">
                                <input
                                    type="text"
                                    name="name"
                                    className="form-control"
                                    placeholder="Name"
                                    required
                                    value={this.state.dataCreditCard.name}
                                    onChange={this.handleInputChange}
                                    onFocus={this.handleInputFocus}
                                />
                                {this.state.formErrorsCreditCard.name && this.state.dataCreditCard.name.length < 7 &&
                                    <div className="help-blockRegUser">Complete Name is required</div>
                                }
                            </div>
                            <div className="row">
                                <div className="col-6">
                                    <input
                                        type="tel"
                                        name="expiry"
                                        className="form-control"
                                        placeholder="Valid Thru"
                                        pattern="\d\d/\d\d"
                                        required
                                        onChange={this.handleInputChange}
                                        onFocus={this.handleInputFocus}
                                    />
                                    <small>Ex.: YY/ MM</small>
                                    {this.state.formErrorsCreditCard.expiry &&
                                        <div className="help-blockRegUser">Valid Expiration Date is required</div>
                                    }
                                </div>

                                <div className="col-6">
                                    <input
                                        type="tel"
                                        name="cvc"
                                        className="form-control"
                                        placeholder="CVC"
                                        pattern="\d{3,4}"
                                        required
                                        onChange={this.handleInputChange}
                                        onFocus={this.handleInputFocus}
                                    />
                                    {this.state.formErrorsCreditCard.cvc && this.state.dataCreditCard.cvc.length < 3 &&
                                        <div className="help-blockRegUser">Complete CVC is required</div>
                                    }
                                </div>
                            </div>
                            <input type="hidden" name="issuer" value={issuer} />
                            <div className="form-actions">
                                <Button className="btn btn-primary btn-block"
                                    disabled={!formValid || loading || !this.state.formValidCreditCard}
                                    onClick={() => payButton(dataCreditCard)}>
                                    {loading ? 'Checking...' : 'PAY'}
                                </Button>
                            </div>
                        </form>
                        {formData && (
                            <div className="App-highlight">
                                {formatFormData(formData).map((d, i) => <div key={i}>{d}</div>)}
                            </div>
                        )}
                    </div>
                </div>
            </Card>

        );
    }
}