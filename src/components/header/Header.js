// import React from 'react'
import React, { Component } from 'react'
// import { userService } from "../../services/user.service";
import * as userService from '../../services/user.service'

import { Navbar, Nav, NavDropdown, Image, Button, Form } from 'react-bootstrap'
import { Animated } from "react-animated-css";
import { isMobile } from 'react-device-detect';

import './header.css'
// import avatar from '../../imgs/avatar.jpg'

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-daterange-picker/dist/css/react-calendar.css' // For some basic styling. (OPTIONAL)
import CustomInputDate from '../../components/dateComponent'
import { store } from 'react-notifications-component';


// export default (props) => {
export default class Header extends Component {

  constructor(props) {
    super(props);

    this.state = {
      logged: false,
      userName: '',
      userId: '',
      userImg: '',
      isGuide: false
    };
  }

  componentDidMount() {
    try {
      const dataUser = JSON.parse(localStorage.getItem('user'))
      if (dataUser) {
        this.setState({
          logged: true,
          userName: dataUser.username,
          userId: dataUser.userid
        })
        userService.getUser(dataUser.userid)
          .then(async res => {
            this.setState({
              userImg: res.picture.link
            })
          }, err => {
            this.showErrMessage('Unable to get user image!')
            console.log(err)
          })
        userService.getUserRoles(dataUser.userid)
          .then(async res => {
            for (let index = 0; index < res.length; index++) {
              if (res[index] === 'GUIDE') {
                this.setState({
                  isGuide: true
                })
              }
            }

          }, err => {
            this.showErrMessage('Unable to verify if user is guide!')
            console.log(err)
          })
      } else {
        console.log("Isn't logged in")
      }
    } catch (error) {
      this.setState({
        logged: false
      })
      this.showErrMessage("Didn't find the token!")
    }
  }

  doLogout = async () => {
    try {
      await userService.logout()
      this.setState({
        logged: false
      })
    } catch (err) {
      // console.log("Unable to log out")
      this.showErrMessage("Unable to log out")
      console.log(err)
    }
  }

  showErrMessage(errMessage) {
    store.addNotification({
      title: "Error!",
      message: errMessage,
      type: "danger",
      insert: "top",
      container: "top-right",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000,
        onScreen: true,
        // pauseOnHover: true,
        showIcon: true,
        touch: true
      }
    });
  }

  render() {
    const {
      showSignInUP,
      showSearchHeader,
      loading,
      locationInput,
      dateInput,
      formValid,
      onSearch,
      handleKeyPress,
      handleLocationInput,
      onCalendarChange,
    } = this.props;
    return (
      <div className="divHeader">

        <Navbar fixed='top' className='header' expand='lg'>
          <Navbar.Brand className='title' href='/'>CICERONE</Navbar.Brand>

          {showSearchHeader && (
            <div>

              {!isMobile && (

                <Animated animationIn="fadeInUp" animationOut="fadeOut" isVisible={showSearchHeader} animationInDelay={1050}>

                  <Form.Row inline className='searchHeader'>
                    <Form.Control type="text" placeholder="Location" className="mr-sm-2 searchHeader"
                      id='locationInput' name='locationInput'
                      value={locationInput}
                      onChange={handleLocationInput}
                      onKeyPress={(event) => handleKeyPress(event)}
                    />

                    <div className='divDateHeader'>
                      <DatePicker
                        value={dateInput}
                        selected={dateInput}
                        onSelect={onCalendarChange} //when day is clicked
                        customInput={<CustomInputDate className="dateSerch" />}
                        // showTimeInput={true}
                        showMonthDropdown
                        showYearDropdown
                        dateFormat="MMMM d, yyyy"
                        // withPortal
                        minDate={(new Date())}
                      />
                    </div>
                    {/* </InputGroup.Text> */}
                    <Button variant="outline-info" className='btnSearchHeader'
                      disabled={!formValid || loading}
                      // onClick={() => this.buttonSearchPress()
                      onClick={() => onSearch()}
                    >
                      {loading ? 'Searching...' : 'Search'}
                    </Button>
                  </Form.Row>


                </Animated>
              )}

            </div>

          )}



          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='ml-auto'>
              {this.state.logged && (
                <Nav.Link className='menuItem' href='/calendar'>calendar</Nav.Link>
              )}
              {/* {this.state.logged && (
                <Nav.Link className='menuItem' href='/discovery'>discovery</Nav.Link>
              )} */}
              {!this.state.logged && showSignInUP !== false && (
                <Nav.Link className='menuItem' href='/login'> Sign in</Nav.Link>
              )}
              {!this.state.logged && showSignInUP !== false && (
                <Button className='buttonSingUp' variant="outline-primary" href='/registerUser'>Sing up</Button>
              )}
              {this.state.logged && (
                <NavDropdown className='dropMenu' title={<div>
                  <Image className='avatar' src={this.state.userImg} alt='user_pic' />
                </div>} id='basic-nav-dropdown' >
                  {/* <NavDropdown.Item className='headerName'>{this.state.userName}</NavDropdown.Item> */}
                  {this.state.isGuide === false && (
                    <div>
                      <NavDropdown.Item className='headerName' >{this.state.userName}</NavDropdown.Item>
                    </div>
                  )}
                  {this.state.isGuide === true && (
                    <div>
                      <NavDropdown.Item className='headerName' href={`/profile/${this.state.userId}`}>{this.state.userName}</NavDropdown.Item>
                      <NavDropdown.Item href='/myTours'>My Tours</NavDropdown.Item>

                    </div>
                  )}
                  {this.state.isGuide === false && (
                    <div>
                      <NavDropdown.Item href='/becomeGuide'>Become Guide</NavDropdown.Item>
                    </div>
                  )}
                  {/* <NavDropdown.Item href='/profile'>Profile</NavDropdown.Item>
                  <NavDropdown.Item href='/becomeGuide'>Become Guide</NavDropdown.Item> */}
                  <NavDropdown.Divider />
                  <NavDropdown.Item className='sign' onClick={this.doLogout}>Sing out</NavDropdown.Item>
                </NavDropdown>
              )}

            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    )
  }
}
