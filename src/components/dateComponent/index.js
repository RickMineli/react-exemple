import React, { Component } from "react";
import { Button } from 'react-bootstrap'

import './dateComponent.css'
export default class CustomInputDate extends Component {

    render() {
        const { value, onClick } = this.props;
        return (
            <Button
                className="customInput"
                onClick={onClick}>
                {value}
            </Button>

        );
    }
}
