import React, { Component } from 'react'
import { Card, FormControl, Button, InputGroup, ListGroup, Col, Row } from 'react-bootstrap'
import './login.css'
/* COLOCAR OS ICONES */
// import TiSocialFacebook from 'react-icons/ti/index'
import * as userService from '../../services/user.service'
import { Redirect } from 'react-router-dom'
import { store } from 'react-notifications-component';


export default class LoginComponent extends Component {


  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      error: "",
      routeFeed: false,
      loading: false,
      errorMessage: '',
      // submittedEmail: false,
      // submittedPassword: false,


      cvc: '',
      expiry: '',
      focus: '',
      name: '',
      number: '',

      //----------------- Validations
      formValid: false,
      formErrors: {
        email: '',
        password: ''
      },
      formCheck: {
        email: '',
        password: ''
      },
    };
  }

  async componentDidMount() {
    try {
      await userService.logout()
      if (!JSON.parse(localStorage.getItem('user'))) {
        console.log("Logout succeeded!")
      }
      this.setState({
        logged: false
      })
    } catch (err) {
      console.log("Could not log out")
      console.log(err)
    }
  }

  tryLogin = async e => {
    this.setState({
      loading: true
    })
    e.preventDefault();

    // const { email, password, returnUrl } = this.state;

    // stop here if form is invalid
    if (!(this.state.email && this.state.password)) {
      console.log('invalido')
      this.setState({
        loading: false,
        password: "",
        submittedEmail: true,
        submittedPassword: true
      })
      return;
    } else {
      await userService.login(this.state.email, this.state.password)
        .then(res => {
          console.log(res.headers)
          this.showSuccessMessage(res.headers.username)
          this.setState({
            routeFeed: true,
            loading: false,
          })
          // this.props.verifyUser(this.state.routeFeed)

        }, err => {
          console.log(err)
          this.handleError(err)
        });
    }
  }

  handleUserInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value },
      () => { this.validateField(name, value) });
  }

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;
    switch (fieldName) {
      case 'email':
        let emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        fieldValidationErrors.email = emailValid ? false : true;
        fieldValidationCheck.email = emailValid ? true : false;
        break;
      case 'password':
        let passwordValid = value.length >= 1;
        fieldValidationErrors.password = passwordValid ? false : true;
        fieldValidationCheck.password = passwordValid ? true : false;
        break;
      default:
        break;
    }
    this.setState({
      formErrors: fieldValidationErrors,
      formCheck: fieldValidationCheck
    }, this.validateForm);
  }

  validateForm() {
    this.setState({
      formValid:
        this.state.formErrors.email === false &&
        this.state.formErrors.password === false
    });
  }

  async handleError(error) {
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      const errorMessage = error.error.message
      return alert(errorMessage)
    } else {
      // console.log(error)
      if (error.response.status === 401) {
        // return alert("Email or password is incorrect")
        errorMessage = "Email or password is incorrect"
        this.showErrMessage(errorMessage)
        fieldValidationErrors.password = true
        fieldValidationCheck.password = false
        this.setState({
          formErrors: fieldValidationErrors,
          formCheck: fieldValidationCheck,
          validateForm: false
        })
      } else if (error.response.status === 404) {
        errorMessage = "Something is wrong, try again later"
        this.showErrMessage(errorMessage)
        fieldValidationErrors.password = true
        fieldValidationCheck.password = false
        this.setState({
          formErrors: fieldValidationErrors,
          formCheck: fieldValidationCheck,
          validateForm: false
        })
      } else {
        return alert(errorMessage)
      }
    }
    this.setState({
      error: errorMessage,
      loading: false,
      password: "",
      submittedPassword: false,
      formValid: false
    })
  }

  showErrMessage(errMessage) {
    store.addNotification({
      title: "Error!",
      message: errMessage,
      type: "danger",
      insert: "top",
      container: "top-right",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000,
        onScreen: true,
        // pauseOnHover: true,
        showIcon: true,
        touch: true
      }
    });
  }

  showSuccessMessage(successMessage) {
    store.addNotification({
      title: "Welcome again:",
      message: successMessage,
      type: "success",
      insert: "bottom",
      container: "top-right",
      animationIn: ['animated', 'fadeIn'],
      animationOut: ["animated", "fadeOut"],
      // width: '300px',
      dismiss: {
        duration: 5000,
        // onScreen: true,
        // pauseOnHover: true,
        // showIcon: true
      }
    });
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      if (this.state.formValid && this.state.email !== '' && this.state.password !== '') {
        this.tryLogin(event)
      }
    }

  }


  render() {
    return (
      <div>    
        {this.state.routeFeed && (
          <Redirect to={{ pathname: '/' }} />
        )}
        <div className='tudoLogin'>
          <div className='cardPositionLogin'>
            <Card className='handleCardLogin' >
              <Card.Title className="coverTitleLogin">Login</Card.Title>
              <div className='formPositionLogin'>
                <InputGroup className='userEmail'>
                  <FormControl
                    type="email"
                    placeholder='Email'
                    aria-label='Username'
                    aria-describedby='basic-addon1'
                    name="email"
                    isInvalid={this.state.formErrors.email}
                    isValid={this.state.formCheck.email}
                    value={this.state.email}
                    onChange={this.handleUserInput}
                    onKeyPress={this.handleKeyPress}
                  // value={this.state.email}
                  // onChange={e => this.setState({ email: e.target.value, submittedEmail: true })}
                  />
                </InputGroup>
                {!this.state.formErrors.email === false && this.state.email &&
                  <div className="help-blockRegUser">Please, type a valid email</div>
                }
              </div>
              <div className='formPositionLogin'>
                <InputGroup className='userPassword'>
                  <FormControl
                    type="password"
                    placeholder='Password'
                    aria-label='Password'
                    name='password'
                    isInvalid={this.state.formErrors.password}
                    isValid={this.state.formCheck.password}
                    value={this.state.password}
                    onChange={this.handleUserInput}
                    onKeyPress={this.handleKeyPress}
                  // value={this.state.password}
                  // onChange={e => this.setState({ password: e.target.value, error: "", submittedPassword: true })}
                  />
                </InputGroup>
                {!this.state.formErrors.password === false && this.state.password.length === 0 &&
                  <div className="help-blockRegUser">Password is required</div>
                }
              </div>
              {/* {this.state.error &&
                <div className='positionErrorMessage'>
                  <div className={'alert alert-danger'} style={{ textAlign: 'center', paddingTop: '5px', paddingBottom: '0px' }}>
                    <h6> {this.state.error}</h6>
                  </div>
                </div>
              } */}
              <div className='buttonPositionSignUp'>
                <Button
                  disabled={!this.state.formValid || this.state.loading}
                  // disabled={this.state.loading || !this.state.password || !this.state.email}
                  onClick={!this.state.loading ? this.tryLogin : null}
                  className='buttonSignUp'
                // style={{ backgroundColor: '#fa6980', borderColor: '#fa6980' }}
                >
                  {this.state.loading ? 'Checking...' : 'Sign in'}
                </Button>
              </div>
              <div>
                <ListGroup.Item className='loginItem' > </ListGroup.Item>
                <ListGroup.Item className='loginItemBotton'> </ListGroup.Item>
              </div>
              <div className='registerOpt' >
                <Row className='rowRegisterPosition'>
                  <div>
                    <Col style={{ padding: '0%' }}>
                      <h6 className='subCoverRegister' style={{ marginBottom: '0px' }}>No account yet?  </h6>
                    </Col>
                  </div>
                  <div>
                    <Col style={{ paddingLeft: '3px', paddingRight: '0%' }}>
                      <a className='subCoverRegister' style={{ color: '#d8546a' }} href='/registerUser'>Create an account here</a>

                    </Col>
                  </div>
                </Row>
                <Row className='rowRegisterPosition'>
                  <div>
                    <Col style={{ padding: '0%' }}>
                      <h6 className='subCoverRegister' style={{ marginBottom: '0px' }}>Back to main page?  </h6>
                    </Col>
                  </div>
                  <div>
                    <Col style={{ paddingLeft: '3px', paddingRight: '0%' }}>
                      <a className='subCoverRegister' style={{ color: '#d8546a' }} href='/'>click here!</a>

                    </Col>
                  </div>
                </Row>
                {/* <div className='SocialIconsLogin'>
                  <a className='resp-sharing-button__link' href='www.ciceroneguide.com.br' target='_blank' rel='noopener noreferrer' aria-label='' >
                    <div className='resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small'><div aria-hidden='true' className='resp-sharing-button__icon resp-sharing-button__icon--solid'>
                      <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24'><path d='M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z' /></svg>
                    </div>
                    </div>
                  </a>


                  <a className="resp-sharing-button__link" href='www.ciceroneguide.com.br' target='_blank' rel='noopener noreferrer' aria-label='' >
                    <div className="resp-sharing-button resp-sharing-button--google resp-sharing-button--small">
                      <div aria-hidden="true" className="resp-sharing-button__icon resp-sharing-button__icon--solid">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11.37 12.93c-.73-.52-1.4-1.27-1.4-1.5 0-.43.03-.63.98-1.37 1.23-.97 1.9-2.23 1.9-3.57 0-1.22-.36-2.3-1-3.05h.5c.1 0 .2-.04.28-.1l1.36-.98c.16-.12.23-.34.17-.54-.07-.2-.25-.33-.46-.33H7.6c-.66 0-1.34.12-2 .35-2.23.76-3.78 2.66-3.78 4.6 0 2.76 2.13 4.85 5 4.9-.07.23-.1.45-.1.66 0 .43.1.83.33 1.22h-.08c-2.72 0-5.17 1.34-6.1 3.32-.25.52-.37 1.04-.37 1.56 0 .5.13.98.38 1.44.6 1.04 1.84 1.86 3.55 2.28.87.23 1.82.34 2.8.34.88 0 1.7-.1 2.5-.34 2.4-.7 3.97-2.48 3.97-4.54 0-1.97-.63-3.15-2.33-4.35zm-7.7 4.5c0-1.42 1.8-2.68 3.9-2.68h.05c.45 0 .9.07 1.3.2l.42.28c.96.66 1.6 1.1 1.77 1.8.05.16.07.33.07.5 0 1.8-1.33 2.7-3.96 2.7-1.98 0-3.54-1.23-3.54-2.8zM5.54 3.9c.33-.38.75-.58 1.23-.58h.05c1.35.05 2.64 1.55 2.88 3.35.14 1.02-.08 1.97-.6 2.55-.32.37-.74.56-1.23.56h-.03c-1.32-.04-2.63-1.6-2.87-3.4-.13-1 .08-1.92.58-2.5zM23.5 9.5h-3v-3h-2v3h-3v2h3v3h2v-3h3" /></svg>
                      </div>
                    </div>
                  </a>
                </div> */}
              </div>
            </Card>
          </div>
        </div>
      </div >
    )
  }
}
