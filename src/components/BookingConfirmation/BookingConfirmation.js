import React, { Component } from 'react'

import './BookingConfirmation.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusCircle, faMinusCircle } from '@fortawesome/free-solid-svg-icons'
import { Col, Card, Form, Button } from 'react-bootstrap'

export default class BookingConfirmation extends Component {
    render() {
        const {
            loading,
            creteNewParticipant,
            removeParticipant,
            participants,
            handlerParticipantInput,
            formValid,
            formErrors,
            formCheck,
            price,
            goPayment,
            payment
        } = this.props;
        return (
            <div>
                <Card className='participantsCard' >
                    {/* <Form.Row className='formRowHowManyBooking'>
                        <Form.Label className="labelParticipantsNumber">How many participants?</Form.Label>
                        <Form.Group as={Col} id="formGridCity" className='formParticipantsNumber'>
                            <Form.Control type='number' placeholder='1' id='participantsNumber' name='participantsNumber' onChange={creteNewParticipant} />
                        </Form.Group>
                    </Form.Row> */}
                    <Form.Row>
                        <Form.Label className="labelParticipantsInstruction">Please insert data of Participants</Form.Label>
                    </Form.Row>
                    {participants.map((participant, index) => (
                        <Col key={index}>
                            <Form.Row>
                                <Form.Label className="labelParticipants">Participant # {index + 1}:</Form.Label>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} id="formGridCity">
                                    <Form.Control className="participantName" type='text' placeholder='Complete Name'
                                        id='name' name='name'
                                        isInvalid={formErrors[index].name}
                                        isValid={formCheck[index].name}
                                        value={participant.name}
                                        onChange={(e) => handlerParticipantInput(e, index)}
                                    />
                                    {formErrors[index].name && participant.name &&
                                        <div className="help-blockRegUser">Full name is required</div>
                                    }
                                </Form.Group>
                                <Form.Group as={Col} id="formGridCity">
                                    <Form.Control className="participantDoc" type='number'
                                        placeholder='Age'
                                        id='age' name='age'
                                        isInvalid={formErrors[index].age}
                                        isValid={formCheck[index].age}
                                        value={participant.age}
                                        onChange={(e) => handlerParticipantInput(e, index)}
                                    />
                                    {formErrors[index].age && participant.age === '' &&
                                        <div className="help-blockRegUser">Age is required</div>
                                    }
                                    {formErrors[index].age && participant.age.length > 3 &&
                                        <div className="help-blockRegUser">Age must be valid</div>
                                    }
                                </Form.Group>
                                <Form.Group as={Col} id="formGridCity">
                                    <Form.Control placeholder='Gender' as="select" name='gender'
                                        isInvalid={formErrors[index].gender}
                                        isValid={formCheck[index].gender}
                                        value={participant.gender}
                                        onChange={(e) => handlerParticipantInput(e, index)}
                                    >
                                        <option value='' >Gender</option>
                                        <option value='Male' >Male</option>
                                        <option value='Female'>Female</option>
                                        <option value='Other'>Other</option>
                                    </Form.Control>
                                    {formErrors[index].gender && participant.gender === '' &&
                                        <div className="help-blockRegUser">Choose a gender is required</div>
                                    }
                                </Form.Group>

                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} id="formGridZip">
                                    <Form.Control className="participantDoc" type='text' placeholder='Document Name (C.P.F./ R.G...)'
                                        id='documentName' name='documentName'
                                        isInvalid={formErrors[index].documentName}
                                        isValid={formCheck[index].documentName}
                                        value={participant.documentName}
                                        onChange={(e) => handlerParticipantInput(e, index)}
                                    />
                                    {formErrors[index].documentName && participant.documentName &&
                                        <div className="help-blockRegUser">Document Name is required</div>
                                    }
                                </Form.Group>
                                <Form.Group as={Col} id="formGridZip">
                                    <Form.Control className="participantDoc" type='text' placeholder='Document Number'
                                        id='document' name='document'
                                        isInvalid={formErrors[index].document}
                                        isValid={formCheck[index].document}
                                        value={participant.document}
                                        onChange={(e) => handlerParticipantInput(e, index)}
                                    />
                                    {formErrors[index].document && participant.document &&
                                        <div className="help-blockRegUser">Document Number is required</div>
                                    }
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                {index > 0 &&
                                    <div className="btnPositionPlusPostReg">
                                        <Button className='btnIconAddRegPost' variant="link"
                                            onClick={() => removeParticipant(participant, index)}
                                        >
                                            <FontAwesomeIcon
                                                className='iconPlusPostReg'
                                                icon={faMinusCircle}
                                            />
                                        </Button>
                                    </div>
                                }
                                {participants.length - 1 === index && participants.length < 10 &&
                                    <div className="btnPositionPlusPostReg">
                                        <Button className='btnIconAddRegPost' variant="link"
                                            onClick={(e) => creteNewParticipant(e, index)}
                                        >
                                            <FontAwesomeIcon
                                                className='iconPlusPostReg'
                                                icon={faPlusCircle}
                                            />
                                        </Button>
                                    </div>
                                }
                            </Form.Row>
                        </Col>
                    ))}
                    {payment === false &&
                        <Form.Row>
                            <Form.Label className="labelBookingTotalPrice">Total Price: ${price * participants.length}</Form.Label>
                            <Button
                                disabled={!formValid || loading}
                                onClick={() => goPayment()}
                                className='buttonBooking'
                            >
                                {loading ? 'Checking...' : 'Continue'}
                            </Button>

                        </Form.Row>

                    }


                </Card>
            </div>
        )
    }
}