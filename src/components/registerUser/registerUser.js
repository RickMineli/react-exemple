import React, { Component } from 'react'
import { Card, Button, InputGroup, ListGroup, Row, Col, Form } from 'react-bootstrap'
import { Redirect } from 'react-router-dom'
import './registerUser.css'
// import { userService } from "../../services/user.service";
import MaskedFormControl from 'react-bootstrap-maskedinput';
import { store } from 'react-notifications-component';

//------------------------------------------- API import
import * as userService from '../../services/user.service'

//------------------------------------------- Firebase imports
import { storage } from '../../services/firebase';
import { Content } from "./styles";

import filesize from "filesize";
//-------------- Components ImagesHandler
import PrincipalFileList from "../../components/FirebaseComponents/PrincipalFileList";
import PrincipalUpload from '../../components/FirebaseComponents/PrincipalUpload';

// //------------------------------------------- Timer imports
// import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";
// import 'react-daterange-picker/dist/css/react-calendar.css' // For some basic styling. (OPTIONAL)
// import CustomInputDate from '../../components/dateComponent'

export default class RegisterUser extends Component {
  constructor(props) {
    super(props)

    this.state = {
      id: '',
      completeName: "",
      dateBirthday: new Date(),
      gender: "",
      cellPhoneNumber: '',
      email: "",
      city: "",
      state: "",
      country: "",
      postalCode: "",
      description: "",
      registrationNumber: '',
      cadasturCertification: "",
      expeditionDate: null,
      expirationDate: null,
      password: "",
      cpassword: "",
      mainImage: {
        link: "null",
        fireBaseId: "null"
      },

      dateBirthDayString: '____-__-__',

      // showCalendar: false,
      // date: new Date(),
      // dateToPresent: "Date of Birthday",
      redirectFeed: false,
      isBackToMyTours: false,
      loading: false,
      errorMessage: '',

      //----- Img to FirebaseStorage
      uploadMainFile: [],
      uploadedFile: [],
      countUploadedImages: 0,
      countIndexImg: 0,
      mainImgUploadFinished: false,

      //----------------- Validations
      formValid: false,
      formErrors: {
        completeName: '',
        email: '',
        password: '',
        cpassword: '',
        gender: '',
        dateBirthday: '',
        cellPhoneNumber: '',
        city: '',
        state: '',
        country: '',
        postalCode: '',
        description: '',
        registrationNumber: '',
        cadasturCertification: '',
        expeditionDate: '',
        expirationDate: '',
        mainImage: ''
      },
      formCheck: {
        completeName: '',
        email: '',
        password: '',
        cpassword: '',
        gender: '',
        dateBirthday: '',
        cellPhoneNumber: '',
        city: '',
        state: '',
        country: '',
        postalCode: '',
        description: '',
        registrationNumber: '',
        cadasturCertification: '',
        expeditionDate: '',
        expirationDate: '',
        mainImage: ''
      },
    }
  }


  async componentDidMount() {
    if (this.props.becomeGuide) {

      const dataUser = JSON.parse(localStorage.getItem('user'))

      this.setState({
        isBackToMyTours: true
      })

      let fieldValidationErrors = this.state.formErrors;
      let fieldValidationCheck = this.state.formCheck;
      try {
        if (dataUser.userid) {
          fieldValidationErrors.password = false
          fieldValidationCheck.password = true
          fieldValidationErrors.cpassword = false
          fieldValidationCheck.cpassword = true
          fieldValidationErrors.title = false
          fieldValidationCheck.title = true
          fieldValidationErrors.completeName = false
          fieldValidationCheck.completeName = true
          fieldValidationErrors.email = false
          fieldValidationCheck.email = true
          fieldValidationErrors.gender = false
          fieldValidationCheck.gender = true
          fieldValidationErrors.dateBirthday = false
          fieldValidationCheck.dateBirthday = true
          fieldValidationErrors.cellPhoneNumber = false
          fieldValidationCheck.cellPhoneNumber = true
          fieldValidationErrors.city = false
          fieldValidationCheck.city = true
          fieldValidationErrors.state = false
          fieldValidationCheck.state = true
          fieldValidationErrors.country = false
          fieldValidationCheck.country = true
          fieldValidationErrors.postalCode = false
          fieldValidationCheck.postalCode = true
          if (!this.props.becomeGuide) {
            fieldValidationErrors.description = false
            fieldValidationCheck.description = true
            fieldValidationErrors.registrationNumber = false
            fieldValidationCheck.registrationNumber = true
            fieldValidationErrors.cadasturCertification = false
            fieldValidationCheck.cadasturCertification = true
            fieldValidationErrors.expeditionDate = false
            fieldValidationCheck.expeditionDate = true
            fieldValidationErrors.expirationDate = false
            fieldValidationCheck.expirationDate = true
          }

          userService.getUser(dataUser.userid)
            .then(data => {

              if (data.description.length >= 30) {
                fieldValidationErrors.description = false
                fieldValidationCheck.description = true
              } else if (data.description.length > 0 && data.description.length <= 30) {
                fieldValidationErrors.description = true
                fieldValidationCheck.description = false
              }
              if (data.registrationNumber.length >= 4) {
                fieldValidationErrors.registrationNumber = false
                fieldValidationCheck.registrationNumber = true
              }
              if (data.cadasturCertification.length >= 4) {
                fieldValidationErrors.cadasturCertification = false
                fieldValidationCheck.cadasturCertification = true
              }
              if (data.expeditionDate !== null) {
                fieldValidationErrors.expeditionDate = false
                fieldValidationCheck.expeditionDate = true
              }
              if (data.expirationDate !== null) {
                fieldValidationErrors.expirationDate = false
                fieldValidationCheck.expirationDate = true
              }

              let getJustDate = data.dateOfBirth.slice(0, 10)
              let convertDate = new Date(getJustDate + "T00:00:00")

              if (data.expeditionDate) {
                let expeDate = data.expeditionDate.slice(0, 10)
                let expiDate = data.expirationDate.slice(0, 10)
                this.setState({
                  expeditionDate: new Date(expeDate + "T00:00:00"),
                  expeditionDateToString: data.expeditionDate.slice(0, 10),
                  expirationDate: new Date(expiDate + "T00:00:00"),
                  expirationDateToString: data.expirationDate.slice(0, 10),
                })
              }

              this.setState({
                id: data.id,
                completeName: data.name,
                dateBirthday: convertDate,
                dateBirthDayString: getJustDate,
                gender: data.gender,
                cellPhoneNumber: data.cellPhoneNumber,
                email: data.email,
                city: data.city,
                state: data.state,
                country: data.country,
                postalCode: data.postalCode,
                description: data.description,
                registrationNumber: data.registrationNumber,
                cadasturCertification: data.cadasturCertification,
                mainImage: data.picture,

                formErrors: fieldValidationErrors,
                formCheck: fieldValidationCheck,
              }, this.validateForm)

              if (data.picture.link === "null") {
                fieldValidationErrors.mainImage = true
                fieldValidationCheck.mainImage = false
                this.setState({
                  formErrors: fieldValidationErrors,
                  formCheck: fieldValidationCheck,
                }, this.validateForm)
              } else if (data.picture.link !== "null") {
                fieldValidationErrors.mainImage = false
                fieldValidationCheck.mainImage = true

                const imgFile = ({
                  id: data.picture.fireBaseId,
                  name: data.picture.fireBaseId,
                  preview: data.picture.link,
                  progress: 100,
                  uploaded: true,
                  error: false,
                  url: data.picture.link
                });

                this.setState({
                  countUploadedImages: this.state.countUploadedImages + 1,
                  uploadMainFile: this.state.uploadMainFile.concat(imgFile),
                  formErrors: fieldValidationErrors,
                  formCheck: fieldValidationCheck,
                }, this.validateForm)
              }
            }, err => {
              console.log(err)
              this.handleError('unabledataget')
            })
        }
      } catch (error) {
        this.handleError('unabledataget')
        console.log(error)
      }
    }
  }

  tryRegister = async (justUpdate) => {
    this.setState({
      loading: true
    })
    // e.preventDefault();
    // try {
    await userService.register(this.state)
      .then(res => {

        if (this.state.uploadMainFile.length > 0 &&
          this.state.mainImgUploadFinished === false &&
          this.state.countUploadedImages === 0 &&
          justUpdate !== true) {
          this.setState({
            ...res.data
          })
          this.state.uploadMainFile.forEach(this.processUpload)
        } else if (justUpdate !== true) {
          if (!this.props.becomeGuide) {
            this.showSuccessMessage("Welcome to Cicerone:", this.state.completeName)
            this.setState({
              redirectFeed: true,
              loading: false
            })
          } else if (this.props.becomeGuide) {

            userService.updateUserToGuide(this.state.id, this.state)
              .then(res => {
                this.showSuccessMessage("Congratulations!", "Now you're a Cicerone Guide " + this.state.completeName)
                this.setState({
                  redirectFeed: true,
                  loading: false
                })
              }, err => {
                console.log(err)
                this.handleError(err)
              })
          } else if (this.props.updateProfile) {
            this.showSuccessMessage("Congratulations!", "Now you're a Cicerone Guide")
            this.setState({
              redirectFeed: true,
              loading: false
            })
          }

        } else if (justUpdate === true) {
          this.setState({
            authorization: true,
            loading: false
          })
        }


      }, err => {
        this.setState({
          loading: false
        })
        this.handleError(err)
      })
  }



  handleUserInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value },
      () => { this.validateField(name, value) });
  }

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;
    switch (fieldName) {
      case 'completeName':
        let completeNameValid = value.length >= 7 && value.length <= 50;
        fieldValidationErrors.completeName = completeNameValid ? false : true;
        fieldValidationCheck.completeName = completeNameValid ? true : false;
        break;
      case 'email':
        let emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i) && value.length >= 12 && value.length <= 60;
        fieldValidationErrors.email = emailValid ? false : true;
        fieldValidationCheck.email = emailValid ? true : false;
        break;
      case 'password':
        let passwordValid = value.length >= 6 && value.length <= 20;
        fieldValidationErrors.password = passwordValid ? false : true;
        fieldValidationCheck.password = passwordValid ? true : false;

        break;
      case 'cpassword':
        let cpasswordValid = value === this.state.password;
        fieldValidationErrors.cpassword = cpasswordValid ? false : true;
        fieldValidationCheck.cpassword = cpasswordValid ? true : false;
        break;
      case 'gender':
        let genderValid = value !== '' && value.length >= 4 && value.length <= 10;
        fieldValidationErrors.gender = genderValid ? false : true;
        fieldValidationCheck.gender = genderValid ? true : false;
        break;
      case 'cellPhoneNumber':
        let cellPhoneNumberValid = !value.includes('_') && value.length >= 7 && value.length <= 20//value.length >= 9;
        fieldValidationErrors.cellPhoneNumber = cellPhoneNumberValid ? false : true;
        fieldValidationCheck.cellPhoneNumber = cellPhoneNumberValid ? true : false;
        break;
      case 'city':
        let cityValid = value.length >= 5 && value.length <= 40;
        fieldValidationErrors.city = cityValid ? false : true;
        fieldValidationCheck.city = cityValid ? true : false;
        break;
      case 'state':
        let stateValid = value.length >= 2 && value.length <= 40;
        fieldValidationErrors.state = stateValid ? false : true;
        fieldValidationCheck.state = stateValid ? true : false;
        break;
      case 'country':
        let countryValid = value.length >= 2 && value.length <= 40;
        fieldValidationErrors.country = countryValid ? false : true;
        fieldValidationCheck.country = countryValid ? true : false;
        break;
      case 'postalCode':
        let postalCodeValid = value.length >= 7 && value.length <= 20;
        fieldValidationErrors.postalCode = postalCodeValid ? false : true;
        fieldValidationCheck.postalCode = postalCodeValid ? true : false;
        break;

      //-------------------------------------- if become a guide
      case 'description':
        let descriptionValid = value.length >= 30 && value.length <= 1000
        fieldValidationErrors.description = descriptionValid ? false : true;
        fieldValidationCheck.description = descriptionValid ? true : false;
        break;
      case 'registrationNumber':
        let registrationNumberValid = value.length >= 4;
        fieldValidationErrors.registrationNumber = registrationNumberValid ? false : true;
        fieldValidationCheck.registrationNumber = registrationNumberValid ? true : false;
        break;
      case 'cadasturCertification':
        let cadasturCertificationValid = value.length >= 4;
        fieldValidationErrors.cadasturCertification = cadasturCertificationValid ? false : true;
        fieldValidationCheck.cadasturCertification = cadasturCertificationValid ? true : false;
        break;
      default:
        break;
    }
    if (fieldValidationErrors.cpassword === true || fieldValidationErrors.cpassword === false) {
      if (this.state.password !== this.state.cpassword) {
        fieldValidationErrors.cpassword = true
        fieldValidationCheck.cpassword = false
      } else if (this.state.password === this.state.cpassword) {
        if (this.state.password.length !== 0 && this.state.cpassword.length !== 0) {
          fieldValidationErrors.cpassword = false
          fieldValidationCheck.cpassword = true
        }

      }
    }
    if (!this.props.becomeGuide) {
      fieldValidationErrors.description = false
      fieldValidationCheck.description = true
      fieldValidationErrors.registrationNumber = false;
      fieldValidationCheck.registrationNumber = true;
      fieldValidationErrors.cadasturCertification = false;
      fieldValidationCheck.cadasturCertification = true;
    }
    if (this.props.becomeGuide) {
      fieldValidationErrors.password = false;
      fieldValidationCheck.password = true;
      fieldValidationErrors.cpassword = false;
      fieldValidationCheck.cpassword = true;
    }
    this.setState({
      // submittedEmail: true,
      formErrors: fieldValidationErrors,
      formCheck: fieldValidationCheck
    }, this.validateForm);
  }

  handleCalendarChange = async e => {
    const name = e.target.name;
    const date = new Date(e.target.value)
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;

    let expeditionDateValid = !isNaN(date.getTime()) && date < new Date(this.state.expirationDate)
    let expirationDateValid = !isNaN(date.getTime()) && date > new Date(this.state.expeditionDate)
    switch (name) {
      case 'dateBirthday':
        let dateBirthdayValid = !isNaN(date.getTime());
        fieldValidationErrors.dateBirthday = dateBirthdayValid ? false : true
        fieldValidationCheck.dateBirthday = dateBirthdayValid ? true : false
        break;
      case 'expeditionDate':
        fieldValidationErrors.expeditionDate = expeditionDateValid ? false : true
        fieldValidationCheck.expeditionDate = expeditionDateValid ? true : false
        if (date < new Date(this.state.expirationDate)) {
          fieldValidationErrors.expirationDate = false
          fieldValidationCheck.expirationDate = true
        }
        break;
      case 'expirationDate':
        fieldValidationErrors.expirationDate = expirationDateValid ? false : true
        fieldValidationCheck.expirationDate = expirationDateValid ? true : false
        if (date > new Date(this.state.expeditionDate)) {
          fieldValidationErrors.expeditionDate = false
          fieldValidationCheck.expeditionDate = true
        }
        break;
      default:
        break;
    }

    if (!this.props.becomeGuide) {
      fieldValidationErrors.expeditionDate = false
      fieldValidationErrors.expirationDate = false
      fieldValidationCheck.expeditionDate = true
      fieldValidationCheck.expirationDate = true
    }
    this.setState({
      [name]: date,
      formErrors: fieldValidationErrors
    }, this.validateForm)
  };

  validateForm() {
    this.setState({
      formValid:
        this.state.formErrors.completeName === false &&
        this.state.formErrors.email === false &&
        this.state.formErrors.password === false &&
        this.state.formErrors.cpassword === false &&
        this.state.formErrors.gender === false &&
        this.state.formErrors.dateBirthday === false &&
        this.state.formErrors.cellPhoneNumber === false &&
        this.state.formErrors.city === false &&
        this.state.formErrors.state === false &&
        this.state.formErrors.country === false &&
        this.state.formErrors.postalCode === false &&
        this.state.formErrors.mainImage === false &&
        this.state.formErrors.description === false &&
        this.state.formErrors.cadasturCertification === false &&
        this.state.formErrors.registrationNumber === false &&
        this.state.formErrors.expeditionDate === false &&
        this.state.formErrors.expirationDate === false
    });
  }

  async handleError(error) {
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;

    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      const errorMessage = error.error.message
      return alert(errorMessage)
    } else {
      if (error.response.status === 401) {
        errorMessage = "Something is wrong, talk with an Cicerone administrator"
        this.showErrMessage(errorMessage)
      } else if (error.response.status === 500) {
        errorMessage = "something is wrong, talk with an Cicerone administrator"
        this.showErrMessage(errorMessage)
        fieldValidationCheck.email = false
        fieldValidationErrors.email = true
        this.setState({
          formValid: false,
          // email: '',
          formErrors: fieldValidationErrors,
          formCheck: fieldValidationCheck
        })
      }
    }
    this.setState({
      error: errorMessage,
      loading: false,
    })
  }

  //--------------------------------------- Firebase ----------------------------------------
  handleImages = async (imageId, url, ) => {
    const newMainImg = {
      link: url,
      fireBaseId: imageId
    }
    this.setState({
      mainImage: newMainImg,
    })
  }

  handleUpload = async (files) => {
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;
    const uploadedFiles = files.map(file => ({
      file,
      id: "main",
      name: file.name,
      readableSize: filesize(file.size),
      preview: URL.createObjectURL(file),
      progress: 0,
      uploaded: false,
      error: false,
      url: null
    }))
    fieldValidationErrors.mainImage = false
    fieldValidationCheck.mainImage = true
    this.setState({
      uploadMainFile: uploadedFiles,
      formErrors: fieldValidationErrors,
      formCheck: fieldValidationCheck,
    }, this.validateForm)
  }

  updateFile = async (id, data) => {
    this.setState({
      uploadMainFile: this.state.uploadMainFile.map(uploadedFile => {
        return id === uploadedFile.id
          ? { ...uploadedFile, ...data }
          : uploadedFile;
      })
    });
  };

  processUpload = async (image) => {
    const userId = JSON.stringify(this.state.id) //(Math.random().toString(36).substr(1, 21));
    const blob = new Blob([image.file], { type: image.file.type })
    //------------Create new id of image
    const uploadTask = storage.ref(`users/`).child(userId).child(image.id).put(blob);
    uploadTask.on('state_changed',
      (snapshot) => {
        // progrss function ....
        const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        this.updateFile(image.id, { progress: progress });
        this.setState({
          progress: progress,
        })
      },
      (error) => {
        // error function ....
        console.log(error)
        this.updateFile(image.id, {
          error: true
        });
      },
      () => {
        // complete function ....
        storage.ref('users').child(userId).child(image.id).getDownloadURL()
          .then(url => {
            storage.ref('users').child(userId).child(image.id).getMetadata()
              .then(async response => {
                this.updateFile(image.id, {
                  countUploadedImages: this.state.countUploadedImages + 1,
                  uploaded: true,
                  id: image.id,
                  url: url
                });
                await this.handleImages(image.id, url)
                this.setState({
                  mainImgUploadFinished: true
                })
                await this.tryRegister(false)
              }, err => {
                console.log(err)
                this.updateFile(image.id, {
                  error: true
                });
              })
          }, err => {
            console.log(err)
          })
      }
    );
  }

  handleDeleteImage = async (id, principal, deleteLocal) => {
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;
    if (deleteLocal === false) {
      await this.tryRegister(true)
      try {
        if (this.state.authorization === true) {
          if (JSON.parse(localStorage.getItem('user'))) {
            if (JSON.parse(localStorage.getItem('user')).headers.authorization) {
              if (this.state.id) {
                const userId = JSON.stringify(this.state.id)
                const storageRef = storage.ref();
                var desertRef = storageRef.child('users').child(userId).child(id);
                // Delete the file
                await desertRef.delete()
                  .then(res => {
                    this.setState({
                      uploadMainFile: this.state.uploadMainFile.filter(file => file.id !== id)
                    });
                    if (this.state.id) {
                      console.log("deleted main image")
                      const newMainImg = {
                        // id: this.state.mainImage.id,
                        fireBaseId: "null",
                        link: "null"
                      }
                      fieldValidationErrors.mainImage = true
                      fieldValidationCheck.mainImage = false
                      this.setState({
                        mainImage: newMainImg,
                        formErrors: fieldValidationErrors,
                        formCheck: fieldValidationCheck,
                        countUploadedImages: this.state.countUploadedImages - 1
                      }, this.validateForm)
                      this.tryRegister(true)
                    }
                  }, err => {
                    console.log(err)
                  })
              }
            }
          }
        }
      } catch (error) {
        alert('Não foi possivel deletar a imagem')
        console.log(error)
      }
    } else {
      fieldValidationErrors.mainImage = true
      fieldValidationCheck.mainImage = false
      this.setState({
        uploadMainFile: this.state.uploadMainFile.filter(file => file.id !== id),
        formErrors: fieldValidationErrors,
        formCheck: fieldValidationCheck
      }, this.validateForm);

      console.log('delete local required')
    }


  }

  // ------------------------------------------------- Messages (Error/Success) ----------------------------------------
  showErrMessage(errMessage) {
    store.addNotification({
      title: "Error!",
      message: errMessage,
      type: "danger",
      insert: "top",
      container: "top-right",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000,
        onScreen: true,
        pauseOnHover: true,
        showIcon: true

      }
    });
  }

  showSuccessMessage(messageTitle, successMessage) {
    store.addNotification({
      title: messageTitle,
      message: successMessage,
      type: "success",
      insert: "bottom",
      container: "top-right",
      animationIn: ['animated', 'fadeIn'],
      animationOut: ["animated", "fadeOut"],
      // width: '300px',
      dismiss: {
        duration: 5000,
        // onScreen: true,
        // pauseOnHover: true,
        // showIcon: true
      }
    });
  }

  render() {
    const { uploadMainFile } = this.state;
    const { becomeGuide } = this.props
    return (
      <div>
        {this.state.redirectFeed && (
          <Redirect to={{ pathname: '/' }} />
        )}

        {/* {!this.state.loading && ( */}
        <div className='backGroundRegister'>
          <div className='cardPositionRegUser'>
            <Card className='handleCardRegUser' >
              {!becomeGuide &&
                <Card.Title className="coverTitleRegisterUser">User register</Card.Title>
              }
              {becomeGuide &&
                <Card.Title className="coverTitleRegisterUser">Become Guide</Card.Title>
              }
              <div className='formPositionUserRegAling'>
                <InputGroup className='userRegFields'>
                  <Form.Control type='text' placeholder='Full name' id='completeName' name="completeName"
                    // onChange={e => this.setState({ completeName: e.target.value, submittedCompleteName: true })}
                    // value={this.state.completeName}
                    isInvalid={this.state.formErrors.completeName}
                    isValid={this.state.formCheck.completeName}
                    value={this.state.completeName}
                    onChange={this.handleUserInput}
                  />
                </InputGroup>
                {!this.state.formErrors.completeName === false && this.state.completeName &&
                  <div className="help-blockRegUser">Full name is required</div>
                }
              </div>
              {becomeGuide &&

                <div className='formPositionUserRegAling'>
                  <InputGroup className='userRegFields'>
                    <Form.Control type='email' placeholder='E-mail' id='email' name="email"
                      // onChange={this.updateFields}
                      // value={this.state.email} 
                      isInvalid={this.state.formErrors.email}
                      isValid={this.state.formCheck.email}
                      value={this.state.email}
                      onChange={this.handleUserInput}
                      disabled
                    />
                  </InputGroup>
                  {!this.state.formErrors.email === false && this.state.email &&
                    <div className="help-blockRegUser">Please, type a valid email</div>
                  }
                </div>
              }
              {!becomeGuide &&
                <div>
                  <div className='formPositionUserRegAling'>
                    <InputGroup className='userRegFields'>
                      <Form.Control type='email' placeholder='E-mail' id='email' name="email"
                        // onChange={this.updateFields}
                        // value={this.state.email} 
                        isInvalid={this.state.formErrors.email}
                        isValid={this.state.formCheck.email}
                        value={this.state.email}
                        onChange={this.handleUserInput}
                      />
                    </InputGroup>
                    {!this.state.formErrors.email === false && this.state.email &&
                      <div className="help-blockRegUser">Please, type a valid email</div>
                    }
                  </div>

                  <div className='formPositionUserRegAling'>
                    <InputGroup className='userRegFields'>
                      <Form.Control type='password' placeholder='Password' id='password' name='password'
                        // onChange={this.updateFields} 
                        // value={this.state.password} 
                        isInvalid={this.state.formErrors.password}
                        isValid={this.state.formCheck.password}
                        value={this.state.password}
                        onChange={this.handleUserInput}
                      />
                    </InputGroup>
                    {!this.state.formErrors.password === false && this.state.password.length === 0 &&
                      <div className="help-blockRegUser">Password is required</div>
                    }
                    {!this.state.formErrors.password === false && this.state.password &&
                      <div className="help-blockRegUser">Password must be at least 6 characters</div>
                    }
                  </div>
                  <div className='formPositionUserRegAling'>
                    <InputGroup className='userRegFields'>
                      <Form.Control type='password' placeholder='Confirm Password' id='cpassword' name='cpassword'
                        // onChange={this.updateFields}
                        // value={this.state.cpassword}
                        isInvalid={this.state.formErrors.cpassword}
                        isValid={this.state.formCheck.cpassword}
                        value={this.state.cpassword}
                        onChange={this.handleUserInput}
                      />
                    </InputGroup>
                    {!this.state.formErrors.cpassword === false && this.state.cpassword !== this.state.password &&
                      <div className="help-blockRegUser">Passwords do not match</div>
                    }
                  </div>

                </div>
              }


              {/* ---------------------------------------- Sex/ DateBirthday-------------------------------------------------------------- */}
              <div className="formPositionUserRegAling">
                <Form.Label className="labelTimePickerRegUser">Date of Birthday:</Form.Label>
                <MaskedFormControl value={this.state.dateBirthDayString} type='tel' name='dateBirthday' mask='1111-11-11' placeholder='yyyy/mm/dd' onChange={this.handleCalendarChange} />
                {this.state.formErrors.dateBirthday && Object.prototype.toString.call(this.state.dateBirthday) === "[object Date]" &&
                  <div className="help-blockRegPost">Date of Birth is required and must be valid</div>
                }
              </div>
              <div className="formPositionUserRegAling">
                <Form.Group className='alingSexRegUser'  >
                  <Form.Control placeholder='Gender' as="select" name='gender'
                    isInvalid={this.state.formErrors.gender}
                    isValid={this.state.formCheck.gender}
                    value={this.state.gender}
                    onChange={this.handleUserInput}
                  >
                    <option value='' >Gender</option>
                    <option value='Male' >Male</option>
                    <option value='Female'>Female</option>
                    <option value='Other'>Other</option>
                  </Form.Control>
                  {!this.state.formErrors.gender === false && !this.state.gender !== '' &&
                    <div className="help-blockRegUser">Choose a gender is required</div>
                  }
                </Form.Group>
              </div>
              {/* ----------------------------------------------------------------------------------------- */}
              <div className='formPositionUserRegAling'>
                <InputGroup className='userRegFields'>
                  <MaskedFormControl type='tel' value={this.state.cellPhoneNumber} name='cellPhoneNumber' mask='+11 (11) 11111-1111' placeholder='+55 (99) 9 9999-9999'
                    // value={this.state.cellPhoneNumber}
                    onChange={this.handleUserInput} />
                </InputGroup>
                {!this.state.formErrors.cellPhoneNumber === false && this.state.cellPhoneNumber.length === 0 &&
                  <div className="help-blockRegUser">Cell Number is required.</div>
                }
                {!this.state.formErrors.cellPhoneNumber === false && this.state.cellPhoneNumber &&
                  <div className="help-blockRegUser">Cell Number must have at least 9 Characters</div>
                }
              </div>
              <div className='formPositionUserRegAling'>
                <InputGroup className='userRegFields'>
                  <Form.Control type='text' placeholder='City' id='city' name='city'
                    // onChange={this.updateFields} 
                    // value={this.state.city} 
                    isInvalid={this.state.formErrors.city}
                    isValid={this.state.formCheck.city}
                    value={this.state.city}
                    onChange={this.handleUserInput}
                  />
                </InputGroup>
                {!this.state.formErrors.city === false && this.state.city &&
                  <div className="help-blockRegUser">City is required</div>
                }
              </div>
              <div className='formPositionUserRegAling'>
                <InputGroup className='userRegFields'>
                  <Form.Control type='text' placeholder='State' id='state' name='state'
                    // onChange={this.updateFields} 
                    // value={this.state.state}
                    isInvalid={this.state.formErrors.state}
                    isValid={this.state.formCheck.state}
                    value={this.state.state}
                    onChange={this.handleUserInput}
                  />
                </InputGroup>
                {!this.state.formErrors.state === false && this.state.state &&
                  <div className="help-blockRegUser">State is required</div>
                }
              </div>
              <div className='formPositionUserRegAling'>
                <InputGroup className='userRegFields'>
                  <Form.Control type='text' placeholder='Country' id='country' name='country'
                    // onChange={this.updateFields} 
                    // value={this.state.state}
                    isInvalid={this.state.formErrors.country}
                    isValid={this.state.formCheck.country}
                    value={this.state.country}
                    onChange={this.handleUserInput}
                  />
                </InputGroup>
                {!this.state.formErrors.country === false && this.state.country &&
                  <div className="help-blockRegUser">Country is required</div>
                }
              </div>

              <div className='formPositionUserRegAling'>
                <InputGroup className='userRegFields'>
                  <Form.Control type='text' placeholder='Postal Code' id='postalCode' name='postalCode'
                    // onChange={this.updateFields} 
                    // value={this.state.state}
                    isInvalid={this.state.formErrors.postalCode}
                    isValid={this.state.formCheck.postalCode}
                    value={this.state.postalCode}
                    onChange={this.handleUserInput}
                  />
                </InputGroup>
                {!this.state.formErrors.postalCode === false && this.state.postalCode &&
                  <div className="help-blockRegUser">Postal Code is required</div>
                }
              </div>




              {/* --------------------------------------------------------- If become a guide ----------------------------------------------------------------------- */}

              {becomeGuide &&
                <div>

                  <div className='formPositionUserRegAling'>
                    <Card.Title className="coverTitleRegisterUser">Guide Information</Card.Title>

                    <InputGroup className='userRegFields'>
                      <Form.Control type='number' placeholder='Registration number' id='registrationNumber' name='registrationNumber'
                        isInvalid={this.state.formErrors.registrationNumber}
                        isValid={this.state.formCheck.registrationNumber}
                        value={this.state.registrationNumber}
                        onChange={this.handleUserInput}
                      />
                    </InputGroup>
                    {!this.state.formErrors.registrationNumber === false && this.state.registrationNumber.length === 0 &&
                      <div className="help-blockRegUser">A valid Registration Number is required</div>
                    }
                    {!this.state.formErrors.registrationNumber === false && this.state.registrationNumber &&
                      <div className="help-blockRegUser">Registration Number must be at least 4 characters</div>
                    }
                  </div>

                  <div className='formPositionUserRegAling'>
                    <InputGroup className='userRegFields'>
                      <Form.Control type='number' placeholder='Cadastur Certification Number' id='cadasturCertification' name='cadasturCertification'
                        isInvalid={this.state.formErrors.cadasturCertification}
                        isValid={this.state.formCheck.cadasturCertification}
                        value={this.state.cadasturCertification}
                        onChange={this.handleUserInput}
                      />
                    </InputGroup>
                    {!this.state.formErrors.cadasturCertification === false && this.state.cadasturCertification.length === 0 &&
                      <div className="help-blockRegUser">A valid Cadastur Certification Number is required</div>
                    }
                    {!this.state.formErrors.cadasturCertification === false && this.state.cadasturCertification &&
                      <div className="help-blockRegUser">Cadastur Certification Number must be at least 4 characters</div>
                    }
                  </div>

                  <div className='formPositionUserRegAling'>
                    <Form.Row>
                      <Form.Group as={Col} controlId="formGridCity" className='formDatesBeGuide'>
                        {/* <Form.Label>Expedition Date</Form.Label> */}
                        <MaskedFormControl type='tel' value={this.state.expeditionDateToString} name='expeditionDate' mask='1111-11-11' placeholder='Expedition Date' onChange={this.handleCalendarChange} />
                        {this.state.formErrors.expeditionDate && Object.prototype.toString.call(this.state.expeditionDate) === "[object Date]" &&
                          <div className="help-blockRegUser">Expedition date must be valid</div>
                        }
                      </Form.Group>
                      <Form.Group as={Col} controlId="formGridZip" className='formDatesBeGuide'>
                        {/* <Form.Label>Expiration Date</Form.Label> */}
                        <MaskedFormControl type='tel' value={this.state.expirationDateToString} name='expirationDate' mask='1111-11-11' placeholder='Expiration Date' onChange={this.handleCalendarChange} />
                        {this.state.formErrors.expirationDate && Object.prototype.toString.call(this.state.expirationDate) === "[object Date]" &&
                          <div className="help-blockRegUser">Expiration date must be valid</div>
                        }
                      </Form.Group>
                    </Form.Row>
                  </div>
                  <div className='formPositionUserRegAling'>
                    <InputGroup className='userRegFields'>
                      <Form.Control as="textarea" type='text' placeholder='Your Guide Description' id='description' name='description'
                        // onChange={this.updateFields} value={this.state.description} 
                        isInvalid={this.state.formErrors.description}
                        isValid={this.state.formCheck.description}
                        value={this.state.description}
                        onChange={this.handleUserInput} />
                    </InputGroup>
                    {!this.state.formErrors.description === false && this.state.description.length < 30 &&
                      <div className="help-blockRegPost">Description must be at least 30 characters</div>
                    }
                    {!this.state.formErrors.description === false && this.state.description.length > 1000 &&
                      <div className="help-blockRegPost">Description limit is 1000 characters</div>
                    }
                  </div>

                </div>

              }
              {/* --------------------------------------------------------------------------------------------------------------------------------------------------- */}

              {/* ------------------------------------------ Principal image ----------------- */}
              <div className='imagesPosition'>
                <Row className='rowImagePositionRegUser'>
                  <div className="divImageRegUser">
                    <Col>
                      <Content>
                        {uploadMainFile.length < 1 && (
                          <PrincipalUpload onUpload={(files) => this.handleUpload(files)} profile={true} />
                        )}
                        {!!uploadMainFile.length && (
                          <PrincipalFileList files={uploadMainFile} onDelete={(files, deleteLocal) => this.handleDeleteImage(files, true, deleteLocal)} />
                        )}
                      </Content>
                    </Col>
                    {this.state.submittedMainImg && this.state.desableByMainImg === '' &&
                      <div className="help-blockRegPost">Prfile Image is required</div>
                    }
                  </div>
                </Row>

              </div>
              {/* -------------------------------------------------------------------------------------------------- */}
              {!becomeGuide &&
                <div>
                  <div className='buttonPositionRegisterUser'>
                    <Button
                      disabled={!this.state.formValid || this.state.loading}
                      // onClick={!this.state.loading ? this.tryLogin : null}
                      // onClick={this.tryRegister}
                      onClick={!this.state.loading ? this.tryRegister : null}

                      className='buttonRegisterUser'
                    // style={{ backgroundColor: '#fa6980', borderColor: '#fa6980' }}
                    >
                      {this.state.loading ? 'Checking...' : 'Register'}
                    </Button>
                  </div>
                  <div >
                    <ListGroup.Item className='regUserItem' > </ListGroup.Item>
                    <ListGroup.Item className='regUserItemBotton'> </ListGroup.Item>
                  </div>
                  <Row className='rowTryLoginPosition'>
                    <div>
                      <Col style={{ padding: '0%' }}>
                        <h6 className='subCoverTrylogin' style={{ marginBottom: '0px', color: '#444343' }}> Already have an account? </h6>
                      </Col>
                    </div>

                    <div>
                      <Col style={{ paddingLeft: '3px', paddingRight: '0%' }}>
                        <a className='subCoverTrylogin' style={{ color: '#d8546a' }} href='/login'>Try to Log In.</a>

                      </Col>
                    </div>
                  </Row>
                </div>
              }
              {becomeGuide &&
                <div>
                  <div className='buttonPositionRegisterUser'>
                    <Button
                      disabled={!this.state.formValid || this.state.loading}
                      // onClick={!this.state.loading ? this.tryLogin : null}
                      // onClick={this.tryRegister}
                      onClick={!this.state.loading ? this.tryRegister : null}

                      className='buttonRegisterUser'
                    // style={{ backgroundColor: '#fa6980', borderColor: '#fa6980' }}
                    >
                      {this.state.loading ? 'Checking...' : 'Send request'}
                    </Button>
                  </div>
                </div>
              }
            </Card>
          </div>
        </div>
        {/* )} */}
      </div >
    )
  }
}
