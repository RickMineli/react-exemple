import React, { Component } from 'react'
import { Card, Row, CardDeck, Button } from 'react-bootstrap'
import { faClock } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import moment from 'moment'


import './trending.css'
// import image1 from '../../imgs/camping.jpg'
// import image2 from '../../imgs/landscape.jpg'
// import image3 from '../../imgs/creteboat.jpg'
// import image4 from '../../imgs/beach.jpg'

export default class trending extends Component {
  render() {
    const { trandingTours } = this.props
    return (
      <div className='backgroundTrending'>
        <div className='divPositionsFeed'>
          <Row>
            <Card.Title className="titleTrending">trending</Card.Title>
            {/* <h1 className='coverTitles'>Find the best place to meet and discover</h1> */}
          </Row>
        </div>
        <div className='divTrending'>
          <div className='divPositionsFeed'>
            <CardDeck>
              {trandingTours.map((tour, index) => (
                <Card key={index} style={{ position: 'inherit', border: '0px' }} >
                  <Button href={`/tour/${tour.id}`} variant="link" className='buttonLinkTrending'>
                    <Card.Img className='cardImgTrending' variant="top" src={tour.mainImage.link} />
                  </Button>
                  <Card.Body className='bodyTrending'>
                    <Card.Title className='titleStyle'>{tour.title}</Card.Title>
                    <Card.Text className='descriptionCard'>
                      {tour.description}
                    </Card.Text>
                    <Row>
                      <Card.Text className='iconCard'>
                        <FontAwesomeIcon icon={faClock} />
                      </Card.Text>
                      <Card.Text className='timeCard'>
                        {`${moment(Date.parse(new Date(tour.creationDate))).format("MMM DD YYYY")}`}
                      </Card.Text>
                    </Row>
                  </Card.Body>
                </Card>
              ))}
            </CardDeck>
          </div>
        </div>
      </div>

    )
  }
}
