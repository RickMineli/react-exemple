import React, { Component } from "react";

import Dropzone from "react-dropzone";

import { DropContainer, UploadMessage } from "./styles";

export default class Upload extends Component {
  renderDragMessage = (isDragActive, isDragReject) => {
    if (!isDragActive) {
      return <UploadMessage style={{ color: 'white' }}>Drag Other images here...</UploadMessage>;
    }

    if (isDragReject) {
      return <UploadMessage type="error" style={{ color: 'white' }}>File not supported (just images)</UploadMessage>;
    }

    return <UploadMessage type="success" style={{ color: 'white' }}>Drop the files here (just images)</UploadMessage>;
  };


  render() {
    const { onUpload } = this.props;

    return (
      <Dropzone multiple={false} accept="image/*" onDropAccepted={onUpload}>
        {({ getRootProps, getInputProps, isDragActive, isDragReject }) => (
          <DropContainer
            {...getRootProps()}
            isDragActive={isDragActive}
            isDragReject={isDragReject}
          >
            <input {...getInputProps()} />
            {this.renderDragMessage(isDragActive, isDragReject)}
          </DropContainer>
        )}
      </Dropzone>
    );
  }
}
