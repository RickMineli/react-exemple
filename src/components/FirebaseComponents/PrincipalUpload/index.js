import React, { Component } from "react";

import Dropzone from "react-dropzone";

import { DropContainer, UploadMessage } from "./styles";

export default class PrincipalUpload extends Component {
  renderDragMessage = (isDragActive, isDragReject) => {
    if (!isDragActive && !this.props.profile) {
      return <UploadMessage style={{ color: 'white' }}>Drag Main image here (Required)</UploadMessage>;
    }

    if (this.props.profile) {
      return <UploadMessage style={{ color: 'white' }}>Drag Profile image here (Required)</UploadMessage>;

    }
    if (isDragReject) {
      return <UploadMessage type="error" style={{ color: 'white' }}>File not supported (just images)</UploadMessage>;
    }

    return <UploadMessage type="success" style={{ color: 'white' }}>Drop the file here (just images)</UploadMessage>;
  };


  render() {
    const { onUpload } = this.props;

    return (
      <Dropzone multiple={false} accept="image/*" onDropAccepted={onUpload}>
        {({ getRootProps, getInputProps, isDragActive, isDragReject }) => (
          <DropContainer
            {...getRootProps()}
            isDragActive={isDragActive}
            isDragReject={isDragReject}
          >
            <input {...getInputProps()} />
            {this.renderDragMessage(isDragActive, isDragReject)}
          </DropContainer>
        )}
      </Dropzone>
    );
  }
}
