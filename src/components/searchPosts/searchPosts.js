import React, { Component } from 'react'
import { Form, InputGroup } from 'react-bootstrap'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'

import './searchPosts.css'

export default class searchPosts extends Component {
  render () {
    return (
      <div>
        <InputGroup placeholder='Search...' onChange={this.filterList} className='locateForm'>
          <InputGroup.Prepend>
            <InputGroup.Text id='inputGroupPrepend'><FontAwesomeIcon className='iconColor' icon={faMapMarkerAlt} /></InputGroup.Text>
          </InputGroup.Prepend>
          <Form.Control
            type='text'
            placeholder='Location'
            aria-describedby='inputGroupPrepend'
            required
          />
        </InputGroup>
      </div>
    )
  }
}
