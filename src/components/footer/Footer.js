import React, { Component } from 'react'
/*
import TiSocialFacebook from 'react-icons/lib/ti/social-facebook'
import TiSocialTwitter from 'react-icons/lib/ti/social-twitter'
import TiSocialInstagram from 'react-icons/lib/ti/social-instagram'
import TiSocialYoutube from 'react-icons/lib/ti/social-youtube'
 */
import './footer.css'

export default class Footer extends Component {
  render () {
    return (
      <div>
        <br />

        <footer className='footer-distributed'>

          <div className='footer-left'>
          </div>

          <div className='footer-center'>

            <div>
              <i className='fa fa-phone' />
              <p><span>Av. Gedner, 1610</span> Brazil, Paraná</p>
            </div>

            <div>
              <i className='fa fa-phone' />
              <p>+55 44 3027-6360</p>
            </div>

            <div>
              <i className='fa fa-envelope' />
              <p><a href='mailto:support@company.com'>adriaanct@hotmail.com</a></p>
            </div>

          </div>

          <div className='footer-right'>

            <p className='footer-company-about'>
              <span>About the company</span>
              Este sistema serve para quem  deseja encotrar um guia e para quem quer guiar
            </p>

            <div className='footer-icons'>

              {/*<a href='/'><TiSocialFacebook /></a>
              <a href='/'><TiSocialTwitter   /></a>
              <a href='/'><TiSocialInstagram /></a>
              <a href='/'><TiSocialYoutube   /></a> */}

            </div>

          </div>

        </footer>
      </div>
    )
  }
}
