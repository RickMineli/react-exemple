import { INCREMENT, DECREMENT } from '../actions/'

const counterReducer = (state = 0, action) => {
    switch (action.type) {
        case INCREMENT:
            // console.log(state)
            return state += 1
            // break;
        case DECREMENT:
            // console.log(state)
            return state -= 1
            // break
        default:
            return state;
    }
}

export default counterReducer;