export const INCREMENT = 'INCREMENT'
export const DECREMENT = 'DECREMENT'


export const incrementCount = () => {
    // console.log(INCREMENT)

    return {
        type: INCREMENT
    }
}   

export const decrementCount = () => {
    // console.log(DECREMENT)
    return {
        type: DECREMENT
    }
}