import firebase from 'firebase/app'
import 'firebase/storage'

var firebaseConfig = {
  apiKey: 'AIzaSyBoQ__BkJaFDgPH2EZoSgQeb5fT1wR1Hz0',
  authDomain: 'ciceroneguideapp.firebaseapp.com',
  databaseURL: 'https://ciceroneguideapp.firebaseio.com',
  projectId: 'ciceroneguideapp',
  storageBucket: 'ciceroneguideapp.appspot.com',
  messagingSenderId: '625119037581',
  appId: '1:625119037581:web:9783be7eb3795bb3'
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)

const storage = firebase.storage()

export {
  storage, firebase as default
}
