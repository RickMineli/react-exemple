import { storage } from './firebase';
import React, { Component } from 'react'
var progress = 0;

const uploadImage = (image, postId = {}) => {
    const authorization = JSON.parse(localStorage.getItem('user')).headers.authorization
    const randomId = image.name + (Math.random().toString(36).substr(1, 21));

    // const {image} = this.state;
    return new Promise((resolve, reject) => {
        if (authorization) {
            // console.log("antes de inserir", image)
            const blob = new Blob([image], { type: image.type })
            //------------Create new id of image
            const uploadTask = storage.ref(`tours/`).child(postId).child(randomId).put(blob);

            uploadTask.on('state_changed',
                (snapshot) => {
                    // progrss function ....
                    progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                    // this.setState({ progress });
                    // console.log(progress)
                    // progressUpload()
                },
                (error) => {
                    // error function ....
                    console.log(error);
                    reject(error)
                },
                () => {
                    // complete function ....
                    storage.ref('tours').child(postId).child(randomId).getDownloadURL()
                        .then(url => {
                            storage.ref('tours').child(postId).child(randomId).getMetadata()
                                .then(data => {
                                    progress = 0
                                    data.imageId = randomId
                                    data.imageUrl = url
                                    resolve(data)
                                }, err => {
                                    reject(err)
                                })
                        }, err => {
                            reject(err)
                        })
                }
            );
        } else {
            reject('User is not logged!')
        }
    })

}

export const progressUpload = () => {
    return {
        prog: progress
    }
}



export default class ProgressShow extends Component {

    state = {
        newProg: progressUpload
    }
    // componentWillMount() {
    //     if (progressUpload > 5) {
    //         this.setState({
    //             newProg: progressUpload()
    //         })
    //     }
    // }

    render() {
        return (
            <div style={{ paddingTop: '90px' }}>
                <p>{this.state.newProg}</p>
                {/* <p>
                    <button onClick={() => this.props.incrementCount()}> + </button>
                    <button onClick={() => this.props.decrementCount()} > - </button>

                </p> */}
            </div>
        )
    }
}



// export default connect(progressUpload())

export {
    uploadImage,
    // progressUpload
}