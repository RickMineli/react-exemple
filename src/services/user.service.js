import urlApi from './urlApi.service'
const axios = require('axios')



function login(email, password) {
  const loginUser = {
    email: email,
    senha: password
  }
  return new Promise((resolve, reject) => {
    axios.post(urlApi + '/login', loginUser)
      // .then(handleResponse)
      .then(user => {
        if (user) {
          // store user details and basic auth credentials in local storage
          // to keep user logged in between page refreshes
          user.userid = user.headers.userid
          user.username = user.headers.username
          user.authdata = window.btoa(email + ':' + password)
          localStorage.setItem('user', JSON.stringify(user))
          resolve(user)
        }
      }, err => {
        reject(err)
      })
  })
}

function logout() {
  // remove user from local storage to log user out
  localStorage.removeItem('user')
}

function register(value) {
  if (!value.id) {
    const userData = {
      name: value.completeName,
      dateOfBirth: value.dateBirthday,
      gender: value.gender,
      cellPhoneNumber: value.cellPhoneNumber,
      city: value.city,
      state: value.state,
      country: value.country,
      postalCode: value.postalCode,
      description: value.description,
      registrationNumber: value.registrationNumber,
      cadasturCertification: value.cadasturCertification,
      expeditionDate: value.expeditionDate,
      expirationDate: value.expirationDate,
      email: value.email,
      password: value.password,
      imageDTO: value.mainImage
    }
    // console.log(userData)
    return new Promise((resolve, reject) => {
      axios.post(urlApi + '/user', userData)
        // .then(handleResponse)
        .then(user => {
          if (user) {
            login(userData.email, userData.password)
            // user.username = user.data.name
            // user.authdata = window.btoa(value.email + ':' + value.password)
            // localStorage.setItem('user', JSON.stringify(user))
            resolve(user)
          }
        }, err => {
          reject(err)
        })
    })
  }
  if (value.id) {
    const authorization = JSON.parse(localStorage.getItem('user'))

    const userData = {
      name: value.completeName,
      dateOfBirth: value.dateBirthday,
      gender: value.gender,
      cellPhoneNumber: value.cellPhoneNumber,
      city: value.city,
      state: value.state,
      country: value.country,
      postalCode: value.postalCode,
      description: value.description,
      registrationNumber: value.registrationNumber,
      cadasturCertification: value.cadasturCertification,
      expeditionDate: value.expeditionDate,
      expirationDate: value.expirationDate,
      email: value.email,
      imageDTO: value.mainImage
    }
    return new Promise((resolve, reject) => {
      axios.put(urlApi + '/user/' + value.id, userData, { headers: { "Authorization": authorization.headers.authorization } })
        // .then(handleResponse)
        .then(user => {
          if (user) {
            resolve(user)
          }
        }, err => {
          reject(err)
        })
    })
  }


}

const updateUserToGuide = (userId, value) => {
  const authorization = JSON.parse(localStorage.getItem('user'))

  const userData = {
    someData: "someData"
  }
  return new Promise((resolve, reject) => {
    axios.post(urlApi + '/user/upgrade/', userData, { headers: { "Authorization": authorization.headers.authorization } })
      .then(
        res => resolve(res),
        err => reject(err)
      )
  })
}

const getUserRoles = (id) => {
  return new Promise((resolve, reject) => {
    axios.get(urlApi + '/user/' + id + '/role')
      .then(
        res => resolve(res.data),
        err => reject(err)
      )
  })
}


const getUser = (id) => {
  return new Promise((resolve, reject) => {
    axios.get(urlApi + '/user/' + id)
      .then(
        res => resolve(res.data),
        err => reject(err)
      )
  })
}

export {
  login,
  logout,
  getUser,
  register,
  updateUserToGuide,
  getUserRoles
}
