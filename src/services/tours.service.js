import urlApi from "./urlApi.service";
const axios = require('axios')

const getAllTours = (query = {}) => {
    // return axios.get(urlApi + '/tours')
    return new Promise((resolve, reject) => {
        axios.get(urlApi + '/tour')
            .then(
                res => resolve(res),
                err => reject(err)
            )
    })

}

const getTour = (id) => {
    // return axios.get(urlApi + '/tour/' + id)
    return new Promise((resolve, reject) => {
        axios.get(urlApi + '/tour/' + id)
            .then(
                res => resolve(res),
                err => reject(err)
            )
    })
}

const getTourFeedback = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(urlApi + '/feedback/' + id)
            .then(
                res => resolve(res),
                err => reject(err)
            )
    })
}


const getGuideTours = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(urlApi + '/user/' + id + '/tour/')
            .then(
                res => resolve(res),
                err => reject(err)
            )
    })
}


const getTrending = () => {
    return new Promise((resolve, reject) => {
        axios.get(urlApi + '/tour/random/4')
            .then(
                res => resolve(res),
                err => reject(err)
            )
    })
}

const getNewTripsBig = () => {
    return new Promise((resolve, reject) => {
        axios.get(urlApi + '/tour/recentCreatedTour/5')
            .then(
                res => resolve(res),
                err => reject(err)
            )
    })
}

const searchTours = (region, dateString, paginationNumber) => {
    const date = new Date(dateString)
    let day = date.getDate()
    let month = date.getMonth() + 1
    let year = date.getUTCFullYear()
    return new Promise((resolve, reject) => {
        axios.get(urlApi + '/tour/search/page?region=' + region + '&date=' + year + '-' + month + '-' + day + '&page=' + paginationNumber + '&elementsPerPage=6&orderBy=id&direction=ASC')
            .then(
                res => resolve(res),
                err => reject(err)
            )
    })
}

const saveTour = (tour = {}) => {
    const authorization = JSON.parse(localStorage.getItem('user'))

    if (!tour.id) {
        const newTour = {
            title: tour.title,
            region: tour.region,
            description: tour.description,
            modality: tour.modality,
            price: tour.price,
            maxParticipants: tour.maxParticipants,
            stay: tour.stay,
            transport: tour.transport,
            tourDates: tour.tourDates,
            estimatedHour: tour.estimatedHour,
            //----------imgs
            mainImage: tour.mainImage,
            images: tour.images
        }
        // return axios.tour(urlApi + '/anuncios', anuncioNovo, {  headers: { "Authorization": authorization } })
        return new Promise((resolve, reject) => {
            if (authorization) {
                axios.post(urlApi + '/tour', newTour, { headers: { "Authorization": authorization.headers.authorization } })
                    .then(
                        res => resolve(res),
                        err => reject(err)
                    )
            } else {
                const err = "notloggedin"
                reject(err)
            }
        })
    }
    if (tour.id) {
        const newTour = {
            id: tour.id,
            region: tour.region,
            title: tour.title,
            description: tour.description,
            modality: tour.modality,
            price: tour.price,
            creationDate: tour.creationDate,
            lastEditedDate: new Date(),
            maxParticipants: tour.maxParticipants,
            stay: tour.stay,
            transport: tour.transport,
            tourDates: tour.tourDates,
            estimatedHour: tour.estimatedHour,
            //---------- imgs
            mainImage: tour.mainImage,
            images: tour.images,
            //----------
            guide: null
        }
        return new Promise((resolve, reject) => {
            if (authorization) {
                axios.put(urlApi + '/tour/' + tour.id, newTour, { headers: { "Authorization": authorization.headers.authorization } })
                    .then(
                        res => resolve(res),
                        err => reject(err)
                    )
            } else {
                const err = "notloggedin"
                reject(err)
            }

        })
    }
}

const newTourContract = (data) => {
    const authorization = JSON.parse(localStorage.getItem('user'))

    const newContract = {
        tourId: data.id,
        tourDate: data.dateChosen,
        estimatedHour: data.estimatedHour,
        participants: data.participants,
        region: data.region,
        price: data.participants.length * data.price
    }

    // return axios.tour(urlApi + '/anuncios', anuncioNovo, {  headers: { "Authorization": authorization } })
    return new Promise((resolve, reject) => {
        if (authorization) {
            axios.post(urlApi + '/tourcontract', newContract, { headers: { "Authorization": authorization.headers.authorization } })
                .then(
                    res => resolve(res),
                    err => reject(err)
                )
        } else {
            const err = "notloggedin"
            reject(err)
        }
    })
}

const removeTours = (id) => {
    const authorization = JSON.parse(localStorage.getItem('user')).headers.authorization
    return new Promise((resolve, reject) => {
        axios.delete(urlApi + '/tour/' + id, { headers: { "Authorization": authorization } })
            .then(
                res => resolve(res),
                err => reject(err)
            )
    })
}

export {
    getAllTours,
    getTour,
    getTourFeedback,
    getGuideTours,
    getTrending,
    getNewTripsBig,
    searchTours,
    saveTour,
    newTourContract,
    removeTours
}
