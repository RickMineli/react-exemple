import axios from "axios";
import { getToken } from "./auth.service";

const urlApi = 'http://localhost:9090'  //local
// const urlApi = 'https://aw-cicerone-api.herokuapp.com' //heroku

const api = axios.create({
  baseURL: urlApi
});

api.interceptors.request.use(async config => {
  const token = getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export default api;