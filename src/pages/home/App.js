import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import './App.css'
// ---------------------------- Components
// import Footer from '../../components/footer/Footer'
// import Header from '../../components/header/Header'

// ---------------------------- main page
import Feed from '../../pages/feed'

// ------------------------- Auth Pages
import Login from '../login'
import RegisterUser from '../registerUser'

// -------------------------- Tour Pages
import Tour from '../tourPages/tour'
import Tours from '../tourPages/tours'
import TourRegister from '../tourPages/tourRegister'
import tourSearch from '../../pages/tourPages/tourSearch'
import myTours from '../../pages/tourPages/myTours'

// --------------------------------------------------
import Discovery from '../discovery'
import Calendar from '../calendar'
import Profile from '../profile'
import becomeGuide from '../becomeGuide'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import reducers from '../../services/models/reducers'

// ---------------------------- Notification
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import BookingConfirmation from '../../components/BookingConfirmation/BookingConfirmation'

const store = createStore(reducers)

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ReactNotification />
        <div>

          {/* <head> */}
          <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css' />  {/* Para animação de cmponentes */}
          {/* </head> */}

          <BrowserRouter>
            {/* <Header /> */}
            <Switch>
              <Route path='/' exact component={Feed} />
              <Route path='/tourSearch' exact component={tourSearch} />
              <Route path='/tourSearch/:id' exact component={tourSearch} />

              <Route path='/login' exact component={Login} />
              <Route path='/registerUser' exact component={RegisterUser} />

              <Route path='/tour' exact component={Tour} />
              <Route path='/tour/:id' exact component={Tour} />
              <Route path='/tours' exact component={Tours} />
              <Route path='/tours/tourRegister' exact component={TourRegister} />
              <Route path='/tours/tourRegister/:id' component={TourRegister} />

              <Route path='/myTours' component={myTours} />
              <Route path='/myTourRegister' exact component={TourRegister} />
              <Route path='/myTourRegister/:id' exact component={TourRegister} />

              <Route path='/bookingConfirmation/:id' exact component={BookingConfirmation} />


              <Route path='/discovery' exact component={Discovery} />
              <Route path='/calendar' exact component={Calendar} />
              <Route path='/profile' exact component={Profile} />
              <Route path='/profile/:id' exact component={Profile} />
              <Route path='/becomeGuide' exact component={becomeGuide} />

            </Switch>
            {/* <Footer /> */}
          </BrowserRouter>

        </div>
      </Provider>

    )
  }
}

export default App
