import React, { Component } from 'react'
import LoginComponent from '../../components/login/login'
import './index.css'
import Header from '../../components/header/Header'
import Fade from 'react-reveal/Fade'
import Footer from '../../components/footer/Footer'

export default class Index extends Component {
  render () {
    return (
      <div>
        {/* <Header /> */}
        <Header showSignInUP={false} />
        <Fade>
          <LoginComponent />
        </Fade>
        <Footer />

      </div>

    )
  }
}
