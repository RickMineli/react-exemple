// import React, { Component } from 'react'
// import Calendar from '../../components/calendar/calendar';

// export default class index extends Component {
//   render () {
//     return (
//       <div>  
//         <Header/>   
//           <Calendar/>
//         <Footer/>  
//       </div>
//     )
//   }
// }
import React, { Component } from 'react'
import Calendar from 'react-calendar'
import Clock from 'react-live-clock'
import Avatar from '../../imgs/frog.jpg'
import Pen from '../../imgs/pen.jpg'
import moment from 'moment'
import { Row, Col, Card, Container, ListGroup, Image } from 'react-bootstrap'
import Footer from '../../components/footer/Footer';
import Header from '../../components/header/Header';


import './index.css'
import { string } from 'prop-types';

export default class index extends Component {

  state = {
    date: new Date(),
    dayWeekSelected: string,
    dateSelected: string,
    temperatura: string
  }

  onChange = date => {
    this.setState({ 
      date,
      dayWeekSelected: moment(Date.parse(date)).format("dddd"),
      dateSelected: moment(Date.parse(date)).format("MMMM DD")
     })

    var Clima = require('node-clima');
    var c = new Clima({
      format: 'json',    // required
      units: 'Celsius',  // optional
      apikey: 'fdb2e9c5f71d283cb04a915091ace5c1' // required
    });
    let weather
    c.currentByCityName({
      cityName: 'Maringá',
      callback: function (err, data) {
        weather = JSON.parse(data)
        console.log(weather['main']['temp'])
      }
    });
  }

  render() {

    return (
      <div>
        <Header />
        {/* <h1></h1> */}
        <Container>
          <Row>
            <Col>
              <Card className='painelEsquerdo' text="white" style={{ width: '20rem', height: '35rem' }}>
                <Card.Body >
                  <Card.Title><Clock format={'HH:mm:ss'} ticking={true} timezone={'BR/Pacific'} /> </Card.Title>
                  <ListGroup className='lista' variant="flush" > {this.state.dayWeekSelected} <br></br> {this.state.dateSelected}
                    <ListGroup.Item className='fundo'> <Image className='icone' src={Pen} roundedCircle />Fernando de Noronha</ListGroup.Item>
                    <ListGroup.Item className='fundo'> <Image className='icone' src={Avatar} roundedCircle /> Friedrich Nietzsche</ListGroup.Item>
                  </ListGroup>
                </Card.Body>
              </Card>
            </Col>
            <Col className='painelDireito'>
              <Calendar /*className='calendario'*/
                onChange={this.onChange}
              />
            </Col>
          </Row>
        </Container>
        <Footer />
      </div>
    )
  }
}
