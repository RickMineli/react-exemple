import React, { Component } from 'react'
import Zoom from 'react-reveal/Zoom'
import Header from '../../components/header/Header'
import Footer from '../../components/footer/Footer'

export default class discovery extends Component {
  render() {
    return (
      <div>
        <Header />
        <Zoom>
          <h1>DISCOVERY!</h1>
        </Zoom>
        <Footer />
      </div>
    )
  }
}
