import React, { Component } from 'react'

import Search from '../../components/search/search'
import Trending from '../../components/trending/trending'
import Header from '../../components/header/Header'
import Footer from '../../components/footer/Footer'
import FeedSearchTour from '../../components/FeedSearchTour/FeedSearchTour'
import { isMobile } from 'react-device-detect';

import './feed.css'
import NewTrips from '../../components/newtrips/NewTrips'
// import CounterTest from '../../services/models/counterTest/CounterTest'
import * as serviceTours from '../../services/tours.service'
import { Animated } from "react-animated-css";
import loadingGif from '../../imgs/loadingGifs/loading12.gif'


export default class Index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showTrandingNewTrips: true,
      closeSearchComponent: false,
      isVisibleSearchComponent: true,
      isVisibleSearchHeader: false,
      hideDescription: true,
      heightChange: 340,
      isHeightChange: false,


      locationInput: '',
      dateInput: new Date(),
      paginationNumber: 0,
      trandingTours: [],
      newTripsToursBig: [],
      newTripsToursLittle: [],

      loading: false,
      originalItems: [],
      tours: [],



      data: {
        locationInput: '',
        dateInput: new Date()
      },
      //----------------- Validations
      formValid: false,
      formErrors: {
        locationInput: '',
        dateInput: ''
      },
      formCheck: {
        locationInput: '',
        dateInput: ''
      },
    };

    
  }






  async searchPosts(paginationNumber) {
    // let itemSearch = this.state.locationInput
    this.setState({
      loading: true
    })
    serviceTours.searchTours(this.state.locationInput, this.state.dateInput, paginationNumber)
      .then(async res => {
        let itemSearch = res.data
        console.log(itemSearch)
        try {
          this.setState({
            loading: false,
            tours: itemSearch,
          })
          if (itemSearch) {
            this.setState({
              isVisibleSearchHeader: true,
              isVisibleSearchComponent: false,
            })

            if (isMobile) {
              this.setState({
                hideDescription: false,
              })
            }
            setTimeout(() => {
              this.setState({
                showTrandingNewTrips: false,
                closeSearchComponent: true,
                heightChange: 200,
                isHeightChange: true
              })
            }, 1300);

          }
        } catch (error) {
          this.setState({
            loading: false
          })
          alert('Não foi possivel carregar a lista de Anuncios!')
          console.log(error)
        }
      }, err => {
        console.log(err)
      })
  }

  async componentDidMount() {
    let tmpFile = [
      {
        id: '',
        title: "...",
        description: "...",
        price: "...",
        creationDate: "...",
        mainImage: {
          link: loadingGif,
          fireBaseId: "main"
        },
        guide: {
          name: "..."
        }
      }
    ]

    let tmpTranding = []
    let tmpNewTripBig = []
    let tmpNewTripLittle = []
    for (let index = 0; index < 4; index++) {
      tmpTranding = tmpTranding.concat(tmpFile)
    }
    for (let index = 0; index < 2; index++) {
      tmpNewTripBig = tmpNewTripBig.concat(tmpFile)
    }
    for (let index = 0; index < 3; index++) {
      tmpNewTripLittle = tmpNewTripLittle.concat(tmpFile)
    }
    this.setState({
      trandingTours: tmpTranding,
      newTripsToursBig: tmpNewTripBig,
      newTripsToursLittle: tmpNewTripLittle
    })


    serviceTours.getTrending()
      .then(res => {
        console.log(res)
        if (res.data.length >= 4) {
          this.setState({
            trandingTours: res.data,
          })
          serviceTours.getNewTripsBig()
            .then(res => {
              let newTripsBig = []
              let newTripsLittle = []
              for (let index = 0; index < 2; index++) {
                newTripsBig = newTripsBig.concat(res.data[index])
              }
              if (res.data.length >= 5) {
                for (let index = 0; index < 3; index++) {
                  newTripsLittle = newTripsLittle.concat(res.data[index + 2])
                }
                this.setState({
                  newTripsToursBig: newTripsBig,
                  newTripsToursLittle: newTripsLittle
                })
              }

            }, err => {
              console.log(err)
            })
        }

      }, err => {
        console.log(err)
      })
  }


  async changePage(pageIndex) {
    this.setState({
      paginationNumber: pageIndex
    })
    this.searchPosts(pageIndex)

  }

  handleCalendarChange = async dateInput => {
    let newData = this.state.data;
    newData.dateInput = dateInput
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;
    fieldValidationErrors.dateInput = false
    fieldValidationCheck.dateInput = true
    this.setState({
      dateInput,
      data: newData,
      formErrors: fieldValidationErrors,
      formCheck: fieldValidationCheck,
    }, this.validateForm)
  };

  handleSearchInput = (e) => {
    let newData = this.state.data;
    const name = e.target.name;
    const value = e.target.value;
    newData[name] = value
    this.setState({
      [name]: value,
      data: newData
    },
      () => { this.validateField(name, value) });
  }

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;
    switch (fieldName) {
      case 'locationInput':
        let locationInputValid = value.length >= 1;
        fieldValidationErrors.locationInput = locationInputValid ? false : true;
        fieldValidationCheck.locationInput = locationInputValid ? true : false;
        break;
      default:
        break;
    }
    this.setState({
      formErrors: fieldValidationErrors,
      formCheck: fieldValidationCheck
    }, this.validateForm);
  }

  validateForm() {
    this.setState({
      formValid:
        this.state.formErrors.locationInput === false ||
        this.state.formErrors.dateInput === false
    });
  }
  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      if (this.state.formValid === true) {
        this.searchPosts(0)
      }
    }
  }

  render() {
    const { locationInput,
      dateInput,
      tours,
      isVisibleSearchHeader,
      isVisibleSearchComponent,
      hideDescription,
      heightChange,
      isHeightChange,
      trandingTours,
      newTripsToursBig,
      newTripsToursLittle } = this.state;
    return (
      <div className='alingFeed'>
        <Header showSearchHeader={isVisibleSearchHeader}
          loading={this.state.loading}
          locationInput={locationInput}
          dateInput={dateInput}
          formValid={this.state.formValid}
          onSearch={() => this.searchPosts(0)}
          handleKeyPress={(item) => this.handleKeyPress(item)}
          handleLocationInput={(item) => this.handleSearchInput(item)}
          onCalendarChange={(item) => this.handleCalendarChange(item)}
        />

        {!isMobile && (
          <div>
            {this.state.closeSearchComponent === false && (
              <div>

                <Animated animationOut="fadeOut" isVisible={isVisibleSearchComponent} animationOutDelay={1200}>
                  <Search isHeightChange={isHeightChange}
                    showSearchComponent={isVisibleSearchComponent}
                    loading={this.state.loading}
                    locationInput={locationInput}
                    dateInput={dateInput}
                    formValid={this.state.formValid}
                    onSearch={() => this.searchPosts(0)}
                    handleKeyPress={(item) => this.handleKeyPress(item)}
                    handleLocationInput={(item) => this.handleSearchInput(item)}
                    onCalendarChange={(item) => this.handleCalendarChange(item)}
                  />
                </Animated>
              </div>
            )}
          </div>
        )}

        {isMobile && (
          <Search isHeightChange={isHeightChange}
            heightChange={heightChange}
            hideDescription={hideDescription}
            showSearchComponent={true}
            loading={this.state.loading}
            locationInput={locationInput}
            dateInput={dateInput}
            formValid={this.state.formValid}
            onSearch={() => this.searchPosts(0)}
            handleKeyPress={(item) => this.handleKeyPress(item)}
            handleLocationInput={(item) => this.handleSearchInput(item)}
            onCalendarChange={(item) => this.handleCalendarChange(item)}
          />
        )}
        <div>
          {this.state.showTrandingNewTrips && (
            <div>
              <Trending trandingTours={trandingTours} />
              <NewTrips newTripsToursBig={newTripsToursBig} newTripsToursLittle={newTripsToursLittle} />
            </div>
          )}
          {!this.state.showTrandingNewTrips && (
            <div>
              <FeedSearchTour tours={tours} changePage={(pageIndex) => this.changePage(pageIndex)} />
            </div>
          )}
        </div>
        <Footer />

      </div>

    )
  }
}
