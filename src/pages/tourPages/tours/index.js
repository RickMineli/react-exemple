import React, { Component } from 'react'
import { Row, Form, Table, Col, Container, Card, InputGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import Header from '../../../components/header/Header'
import Footer from '../../../components/footer/Footer'
import Zoom from 'react-reveal/Zoom'

import './index.css'

//-------------- firebase imports
import { storage } from '../../../services/firebase';

/* COLOCAR OS ICONES */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt, faTrashAlt, faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'

import * as serviceTours from '../../../services/tours.service'

export default class index extends Component {
    state = {
        loading: true,
        originalItems: [],
        posts: []
    }

    filterList = async (event) => {
        console.log(event.target.value)
        var updatedList
        updatedList = this.state.originalItems.filter(function (item) {
            return item.title.toLowerCase().search(event.target.value.toLowerCase()) !== -1;
        });
        this.setState({ posts: updatedList });
    }

    async componentDidMount() {
        serviceTours.getAllTours()
            .then(async res => {
                //----------------------------------To order images by original id
                var postsOrderById = [...res.data];
                postsOrderById.sort((a, b) => a.id - b.id);
                postsOrderById.map((item, i) => (<div key={i}> {item.id}</div>))
                try {
                    console.log(res.data)
                    this.setState({
                        loading: false,
                        originalItems: postsOrderById,
                        posts: postsOrderById
                    })
                } catch (error) {
                    this.setState({
                        loading: false
                    })
                    alert('Não foi possivel carregar a lista de Anuncios!')
                    console.log(error)
                }
            }, err => {
                this.setState({
                    loading: false
                })
                alert('Não foi possivel carregar a lista de Anuncios!', err)
                console.log(err)
            })

    }

    remove = async (post) => {
        try {
            await serviceTours.removeTours(post.id)
                .then(res => {
                    console.log("Deleted post with id: " + post.id + " and title:", post.title)
                    this.handleDeleteImages(post)
                }, err => {
                    console.log(err)
                })
            this.componentDidMount()
        } catch (error) {
            alert('Não foi possivel excluir o Produto!')

        }
    }

    handleDeleteImages = async (post) => {
        console.log(post.id)
        try {
            if (post.imgIdMain !== "null") {
                this.deleteOneImage(post.id, post.imgIdMain)
            }
            if (post.imgId1 !== "null") {
                this.deleteOneImage(post.id, post.imgId1)
            }
            if (post.imgId2 !== "null") {
                this.deleteOneImage(post.id, post.imgId2)
            }
            if (post.imgId3 !== "null") {
                this.deleteOneImage(post.id, post.imgId3)
            }
            if (post.imgId4 !== "null") {
                this.deleteOneImage(post.id, post.imgId4)
            }
        } catch (error) {
            alert('Não foi possivel deletar a imagem')
            console.log(error)
        }

    }


    deleteOneImage = async (postId, imageId) => {
        const storageRef = storage.ref();
        var desertRef = storageRef.child('tours').child(JSON.stringify(postId)).child(imageId);
        // Delete the file
        await desertRef.delete()
            .then(res => {
                console.log(res)
                console.log(imageId, 'deleted')
            }, err => {
                console.log(err)
            })
    }

    render() {
        return (
            <div >
                <Header />
                <Zoom>
                    <div >
                        {this.state.loading && (
                            <p>Loading Tours...</p>
                        )}
                        {!this.state.loading && (
                            <div>
                                {/* <Form.Control type='serch' placeholder='Procurar por...' /> */}

                                <InputGroup placeholder='Search...' onChange={this.filterList} className='locateForm'>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text id='inputGroupPrepend'><FontAwesomeIcon className='iconColor' icon={faMapMarkerAlt} /></InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <Form.Control
                                        type='text'
                                        placeholder='Location'
                                        aria-describedby='inputGroupPrepend'
                                        required /></InputGroup>
                                <Link className='btn btn-primary float-right' to='tours/tourRegister'>New Tour</Link>

                                <Table striped bordered hover>
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Data of Post</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.posts.map((post) => (
                                            <tr key={post.id}>
                                                <td>{post.id}</td>
                                                <td>
                                                    <Container>
                                                        <Row >
                                                            <Card.Img variant="top" src={post.mainImage.link} style={{ height: "180px", width: '300px', borderRadius: '6px' }} />
                                                            <Col>
                                                                <Card.Body>
                                                                    <Card.Title>{post.title}</Card.Title>
                                                                    <Card.Subtitle>
                                                                        {post.description}
                                                                    </Card.Subtitle>
                                                                </Card.Body>
                                                            </Col>
                                                        </Row>
                                                    </Container>
                                                    <Link onClick={() => this.remove(post.id)} >
                                                        <FontAwesomeIcon className='iconHeart' icon={faTrashAlt} />
                                                    </Link>
                                                    <Link to={`/tours/tourRegister/${post.id}`} >
                                                        <FontAwesomeIcon className='iconColor2' icon={faPencilAlt} />
                                                    </Link>
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>

                                </Table>
                            </div>
                        )}
                    </div>
                    <Footer />
                </Zoom>
            </div>
        )
    }
}
