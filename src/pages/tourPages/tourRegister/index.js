import React, { Component } from 'react'
import './index.css'
import { Card, Row, Form, Button, Col, InputGroup, ListGroup } from 'react-bootstrap'
import { Redirect } from 'react-router-dom'
import * as serviceTours from '../../../services/tours.service'
import MaskedFormControl from 'react-bootstrap-maskedinput';
import tmpImg from '../../../imgs/ny.jpg'
//------------------------------ Components
import Header from '../../../components/header/Header'


//------------------------------------------- Firebase imports
import { storage } from '../../../services/firebase';
import { Content } from "./styles";
// import { uniqueId } from "lodash";
import filesize from "filesize";
//Components ImagesHandler
import FileList from "../../../components/FirebaseComponents/FileList";
import Upload from '../../../components/FirebaseComponents/Upload';
import PrincipalFileList from "../../../components/FirebaseComponents/PrincipalFileList";
import PrincipalUpload from '../../../components/FirebaseComponents/PrincipalUpload';

/* COLOCAR OS ICONES */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusCircle, faMinusCircle } from '@fortawesome/free-solid-svg-icons'

//------------------------------------------- Timer imports
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-daterange-picker/dist/css/react-calendar.css' // For some basic styling. (OPTIONAL)
import CustomInputDate from '../../../components/dateComponent'

//--------------------------------------------------Handle notifications
import { store } from 'react-notifications-component';

export default class index extends (Component) {


  //   var m = moment().utcOffset(0);
  // m.set({hour:0,minute:0,second:0,millisecond:0})
  state = {
    //----- Permisions
    authorization: false,
    imageBackgrount: tmpImg,
    loading: false,
    showCalendar: false,
    // redirectPosts: false,
    canRedirectPosts: false,
    isBackToMyTours: false,
    imgUploadFinished: false,
    mainImgUploadFinished: false,
    showTime: false,
    minDateArr: new Date(),
    //----- Data of API
    id: '',
    guide: [],
    tourDates: [new Date()],
    title: '',
    region: '',
    description: '',
    modality: '',
    price: '',
    stay: false,
    transport: false,
    maxParticipants: '',
    creationDate: '',
    lastEditedDate: '',
    estimatedHour: '',
    // -- imgs
    imgIdMain: "null",
    imgLinkMain: "null",
    mainImage: {
      link: "null",
      fireBaseId: "null"
    },
    images: [
      {
        link: "null",
        fireBaseId: "null"
      },
      {
        link: "null",
        fireBaseId: "null"
      },
      {
        link: "null",
        fireBaseId: "null"
      },
      {
        link: "null",
        fireBaseId: "null"
      },
      {
        link: "null",
        fireBaseId: "null"
      }
    ],

    //----- Img to FirebaseStorage
    uploadedFiles: [],
    uploadMainFile: [],
    countUploadedImages: 0,
    countIndexImg: 0,

    //----------- Validations
    submittedTitle: false,
    submittedDescription: false,
    submittedPrice: false,
    submittedestimatedHour: false,
    submittedtourDate: false,
    submittedMainImg: false,
    desableByestimatedHour: '',
    desableByMainImg: '',

    //----------------- Validations
    formValid: false,
    formErrors: {
      title: '',
      region: '',
      description: '',
      modality: '',
      price: '',
      tourDate: '',
      estimatedHour: '',
      mainImg: '',
      maxParticipants: ''
    },
    formCheck: {
      title: '',
      region: '',
      description: '',
      modality: '',
      price: '',
      tourDate: '',
      estimatedHour: '',
      mainImg: '',
      maxParticipants: ''
    },
  }

  async componentDidMount() {
    if (this.props.match.path === "/myTourRegister/:id" || this.props.match.path === "/myTourRegister") {
      this.setState({
        isBackToMyTours: true
      })
    }
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;
    try {
      if (this.props.match.params.id) {
        fieldValidationErrors.title = false
        fieldValidationCheck.title = true
        fieldValidationErrors.region = false
        fieldValidationCheck.region = true
        fieldValidationErrors.description = false
        fieldValidationCheck.description = true
        fieldValidationErrors.modality = false
        fieldValidationCheck.modality = true
        fieldValidationErrors.price = false
        fieldValidationCheck.price = true
        fieldValidationErrors.tourDate = false
        fieldValidationCheck.tourDate = true
        fieldValidationErrors.estimatedHour = false
        fieldValidationCheck.estimatedHour = true
        fieldValidationErrors.maxParticipants = false
        fieldValidationCheck.maxParticipants = true

        serviceTours.getTour(this.props.match.params.id)
          .then(res => {
            const data = res.data
            //----------------------------------To order images by original id
            var imgOrderById = [...data.images];
            imgOrderById.sort((a, b) => a.id - b.id);
            imgOrderById.map((item, i) => (<div key={i}> {item.id}
              {item.link} {item.fireBaseId}</div>))

            let tourDates = []
            for (let index = 0; index < data.tourDates.length; index++) {
              tourDates = [
                new Date(data.tourDates[index])
              ]
              this.setState({
                tourDates: this.state.tourDates.concat(tourDates)
              })
            }
            console.log('tour dates', tourDates)
            this.setState({
              loading: false,
              id: data.id,
              guide: data.guide,
              title: data.title,
              region: data.region,
              description: data.description,
              modality: data.modality,
              price: data.price,
              maxParticipants: data.maxParticipants,
              stay: data.stay,
              transport: data.transport,
              // tourDates: data.tourDates,
              estimatedHour: data.estimatedHour,
              creationDate: data.creationDate,
              lastEditedDate: data.lastEditedDate,
              //--------------------------- imgs
              mainImage: data.mainImage,
              images: imgOrderById,//data.images,

              imageBackgrount: data.mainImage.link,
              imgIdMain: data.mainImage.fireBaseId,
              imgLinkMain: data.mainImage.link,

              formErrors: fieldValidationErrors,
              formCheck: fieldValidationCheck,
            }, this.validateForm)
            if (data.mainImage.link === "null" || data.mainImage.link === "MainImageTeste") {
              fieldValidationErrors.mainImg = true
              fieldValidationCheck.mainImg = false
              this.setState({
                imageBackgrount: tmpImg,
                formErrors: fieldValidationErrors,
                formCheck: fieldValidationCheck,
              }, this.validateForm)

            } else {
              fieldValidationErrors.mainImg = false
              fieldValidationCheck.mainImg = true
              this.setState({
                formErrors: fieldValidationErrors,
                formCheck: fieldValidationCheck,
              }, this.validateForm)
            }
            const postId = JSON.stringify(data.id)
            storage.ref('tours').child(postId).listAll()
              .then(async res => {
                // console.log(res)
                const dataImages = res.items
                await dataImages.forEach(this.getURLImages)
              }, err => {
                console.log(err)
              })
          }, err => {
            console.log(err)
            this.handleError('unabledataget')
            // this.handleError(err)
          })
      }
      // else {
      //   this.creteNewDate()
      // }
    } catch (error) {
      this.handleError('unabledataget')
      // alert('Unable to retrieve Post Data')
      console.log(error)
    }
  }

  //---------------------------------------------------- API Data
  updateFields = (event) => {
    this.setState({
      [event.target.id]: event.target.value
    })
  }
  handleSwitchChange = switchEvent => () => {
    // let switchNumber = `switch${nr}`;
    // let switchEvent = nr
    this.setState({
      [switchEvent]: !this.state[switchEvent]
    });
  }

  saveDataApi = async (justUpdateImage) => {
    this.setState({
      loading: true
    })
    let totalFilesCount = this.state.uploadedFiles.length + this.state.uploadMainFile.length
    try {
      // if (!this.state.description) return alert('Informe uma descrição para o Produto!')
      // if (!this.state.price) return alert('Informe um preço para o Produto')
      console.log(this.state)
      await serviceTours.saveTour(this.state)
        .then(async res => {
          this.setState({
            authorization: true
          })
          if (!this.state.id) {
            console.log(res)
            console.log("Post crated", res.data)
            this.setState({
              ...res.data
            })
            console.log('authori', this.state.authorization)
            console.log(this.state.id)
          }
          if (this.state.id) {
            console.log(res)
            console.log("Changed post with id: " + this.state.id + " and description:", this.state.description)
            console.log(this.state.id)
          }
          if (this.state.canRedirectPosts === true &&
            this.state.imgUploadFinished === true &&
            this.state.mainImgUploadFinished === true
          ) {

            this.setState({
              redirectPosts: true,
              loading: true
            })

            if (res.status === 201) {
              const successMessage = "Post created!"
              this.showSuccessMessage(successMessage)
            }
            if (res.status === 200) {
              const successMessage = "Post updated!"
              this.showSuccessMessage(successMessage)
            }
          }
          if (JSON.parse(localStorage.getItem('user'))) {
            if (JSON.parse(localStorage.getItem('user')).headers.authorization) {
              if (this.state.uploadMainFile.length > 0 && justUpdateImage === false &&
                this.state.countUploadedImages !== totalFilesCount) {
                // if () {
                await this.state.uploadMainFile.forEach(this.processUpload)
                // }

              }
              if (this.state.uploadedFiles.length > 0 && justUpdateImage === false &&
                this.state.countUploadedImages !== totalFilesCount) {
                // if () {
                await this.state.uploadedFiles.forEach(this.processUpload)
                // this.setState({
                //   imgUploadFinished: true
                // })
                // }
              }
            }
          }


          if (this.state.countUploadedImages === totalFilesCount && justUpdateImage === false) {
            console.log(justUpdateImage, 'justupdateimage')
            console.log(this.state.countUploadedImages + "===" + totalFilesCount)
            this.setState({
              canRedirectPosts: true,
            })
          }
        }, err => {
          console.log(err)
          // alert('Não é o proprietario do anuncio!')
          this.setState({
            authorization: false
          })
          this.handleError(err)
        })
    } catch (error) {
      this.setState({
        loading: false
      })
      // alert('Não foi possivel salvar o cadastro do produto')
      console.log(error)
    }
  }
  //---------------------------------------------------- FirebaseStorage

  handleImages = async (imageId, url, ) => {
    if (imageId === "main") {
      const newMainImg = {
        id: this.state.mainImage.id,
        link: url,
        fireBaseId: imageId
      }
      this.setState({
        mainImage: newMainImg,
        imgLinkMain: url,
        imgIdMain: imageId,
      })
    } else {
      const index = this.state.countIndexImg

      let items = [...this.state.images];
      let item = { ...items[index] };
      item.fireBaseId = imageId;
      item.link = url;
      items[index] = item;

      this.setState({
        images: items, //this.state.images.concat(newImage),
        countIndexImg: this.state.countIndexImg + 1
      })
    }
    console.log("mainImg", this.state.mainImage)
    console.log("Images", this.state.images)
  }

  getURLImages = image => {
    try {
      // console.log(this.state.dataImgsToAPI)
      const postId = JSON.stringify(this.state.id)
      storage.ref('tours').child(postId).child(image.name).getDownloadURL()
        .then(url => {
          if (image.name === "main") {
            const imgFile = ({
              id: image.name,
              name: image.name,
              preview: url,
              progress: 100,
              uploaded: true,
              error: false,
              url: url
            });
            this.setState({
              uploadMainFile: this.state.uploadMainFile.concat(imgFile)
            })
          } else {
            // uniqueId(JSON.parse(image.name))
            // const randomId = image.name + (Math.random().toString(36).substr(1, 21));
            const imgFile = ({
              id: image.name,
              name: image.name,
              preview: url,
              progress: 100,
              uploaded: true,
              error: false,
              url: url
            });
            this.setState({
              uploadedFiles: this.state.uploadedFiles.concat(imgFile)
            });
          }
          this.handleImages(image.name, url)
          this.setState({
            countUploadedImages: this.state.countUploadedImages + 1
          })
          console.log(this.state.uploadedFiles)
        }, err => {
          console.log(err)
        })
    } catch (error) {
      console.log(error)
    }
  }

  handleUpload = async (files, principal) => {
    // console.log(principal)
    if (principal === true) {
      let fieldValidationErrors = this.state.formErrors;
      let fieldValidationCheck = this.state.formCheck;
      // if (this.state.uploadedFiles[0].id === "1") {
      const uploadedFiles = files.map(file => ({
        file,
        id: "main",
        name: file.name,
        readableSize: filesize(file.size),
        preview: URL.createObjectURL(file),
        progress: 0,
        uploaded: false,
        error: false,
        url: null
      }))
      if (this.state.uploadedFiles) {
        fieldValidationErrors.mainImg = false
        fieldValidationCheck.mainImg = true
        this.setState({
          uploadMainFile: uploadedFiles,
          imageBackgrount: uploadedFiles[0].preview,
          // desableByMainImg: 'desable',
          // submittedMainImg: true,
          formErrors: fieldValidationErrors,
          formCheck: fieldValidationCheck,
        }, this.validateForm)
      }
    } else if (principal === false) {
      const randomId = (Math.random().toString(36).substr(1, 21));
      const uploadedFiles = files.map(file => ({
        file,
        id: randomId,//uniqueId(),
        name: file.name,
        readableSize: filesize(file.size),
        preview: URL.createObjectURL(file),
        progress: 0,
        uploaded: false,
        error: false,
        url: null
      }))
      if (this.state.uploadedFiles) {
        this.setState({
          uploadedFiles: this.state.uploadedFiles.concat(uploadedFiles)
        });
      }
    }
    console.log(this.state.uploadedFiles)

    // uploadedFiles.forEach(this.processUpload);
  };

  updateFile = async (id, data) => {
    if (id === "main") {
      this.setState({
        uploadMainFile: this.state.uploadMainFile.map(uploadedFile => {
          return id === uploadedFile.id
            ? { ...uploadedFile, ...data }
            : uploadedFile;
        })
      });
    } else {
      this.setState({
        uploadedFiles: this.state.uploadedFiles.map(uploadedFile => {
          return id === uploadedFile.id
            ? { ...uploadedFile, ...data }
            : uploadedFile;
        })
      });
    }
  };

  processUpload = async (image) => {
    if (image.uploaded === false) {
      const postId = JSON.stringify(this.state.id) //(Math.random().toString(36).substr(1, 21));
      const authorization = JSON.parse(localStorage.getItem('user')).headers.authorization
      console.log(image)
      if (authorization) {
        // console.log("antes de inserir", image)
        const blob = new Blob([image.file], { type: image.file.type })
        //------------Create new id of image
        const uploadTask = storage.ref(`tours/`).child(postId).child(image.id).put(blob);
        uploadTask.on('state_changed',
          (snapshot) => {
            // progrss function ....
            const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
            this.updateFile(image.id, { progress: progress });
            this.setState({
              progress: progress,
            })
            console.log(progress)
          },
          (error) => {
            // error function ....
            console.log(error)
            this.updateFile(image.id, {
              error: true
            });
          },
          () => {
            // complete function ....
            storage.ref('tours').child(postId).child(image.id).getDownloadURL()
              .then(url => {
                storage.ref('tours').child(postId).child(image.id).getMetadata()
                  .then(async response => {
                    this.updateFile(image.id, {
                      uploaded: true,
                      id: image.id,
                      url: url
                    });
                    await this.handleImages(image.id, url)
                    if (image.id === "main" || this.state.uploadMainFile.length === 0) {
                      this.setState({
                        mainImgUploadFinished: true
                      })
                    }
                    this.setState({ countUploadedImages: this.state.countUploadedImages + 1 })
                    let totalFilesCount = this.state.uploadedFiles.length + this.state.uploadMainFile.length
                    if (this.state.countUploadedImages === totalFilesCount) {
                      // if (this.state.mainImgUploadFinished === true) {
                      console.log("entrou pra savar na api")
                      console.log(this.state.countUploadedImages + "--" + totalFilesCount)

                      await this.saveDataApi(false)

                      // this.setState({
                      //   canRedirectPosts: true,
                      // })
                      // }
                    }
                    console.log(this.state.uploadedFiles)
                  }, err => {
                    console.log(err)
                    this.updateFile(image.id, {
                      error: true
                    });
                  })
              }, err => {
                console.log(err)
              })
          }
        );
      } else {
        alert('User is not logged!')
      }

    }
  }

  handleDelete = async (id, principal, deleteLocal) => {
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;
    if (deleteLocal === false) {
      console.log(this.state.uploadedFiles)
      await this.saveDataApi(true)
      console.log(this.state.authorization)
      try {
        if (this.state.authorization === true) {
          if (JSON.parse(localStorage.getItem('user'))) {
            if (JSON.parse(localStorage.getItem('user')).headers.authorization) {
              if (this.state.id) {
                const postId = JSON.stringify(this.state.id)
                const storageRef = storage.ref();
                var desertRef = storageRef.child('tours').child(postId).child(id);

                // Delete the file
                await desertRef.delete()
                  .then(res => {
                    console.log(res)
                    if (principal === true) {
                      this.setState({
                        uploadMainFile: this.state.uploadMainFile.filter(file => file.id !== id)
                      });
                    } else if (principal === false) {
                      this.setState({
                        uploadedFiles: this.state.uploadedFiles.filter(file => file.id !== id)
                      });
                    }
                    if (this.state.id) {
                      if (id === this.state.imgIdMain) {
                        console.log("deleted main image")
                        const newMainImg = {
                          id: this.state.mainImage.id,
                          fireBaseId: "null",
                          link: "null"
                        }
                        fieldValidationErrors.mainImg = true
                        fieldValidationCheck.mainImg = false
                        this.setState({
                          imageBackgrount: tmpImg,
                          mainImage: newMainImg,
                          formErrors: fieldValidationErrors,
                          formCheck: fieldValidationCheck,
                          countUploadedImages: this.state.countUploadedImages - 1
                        }, this.validateForm)
                        this.saveDataApi(true)
                      } else {
                        console.log("deleted other image")
                        const dataImg = this.state.images.filter(file => file.fireBaseId === id)
                        this.setState({
                          images: this.state.images.filter(file => file.fireBaseId !== id)
                        })
                        const newImg = [{
                          id: dataImg[0].id,
                          link: "null",
                          fireBaseId: "null"
                        }]
                        this.setState({
                          images: this.state.images.concat(newImg),
                          countUploadedImages: this.state.countUploadedImages - 1
                        })
                        console.log("indexDataTest", dataImg[0].id)
                        console.log(this.state.images)
                        this.saveDataApi(true)
                      }
                    }
                  }, err => {
                    console.log(err)
                  })
              }
            }
          }
        }
      } catch (error) {
        alert('Não foi possivel deletar a imagem')
        console.log(error)
      }
    } else {
      if (principal === true) {
        fieldValidationErrors.mainImg = true
        fieldValidationCheck.mainImg = false
        this.setState({
          uploadMainFile: this.state.uploadMainFile.filter(file => file.id !== id),
          formErrors: fieldValidationErrors,
          formCheck: fieldValidationCheck
        }, this.validateForm);
      } else if (principal === false) {
        this.setState({
          uploadedFiles: this.state.uploadedFiles.filter(file => file.id !== id)
        });
      }
      console.log('delete local required')
    }


  }


  // componentWillUnmount() {
  //   this.state.uploadedFiles.forEach(file => URL.revokeObjectURL(file.preview));
  // }


  //------------------------------- NewDate
  creteNewDate = async () => {
    // const item = [
    //   new Date()
    // ]

    // // console.log(item)
    // await this.setState({
    //   tourDates: item
    // })
    // console.log(this.state.tourDates)
    const item2 = [
      new Date()
    ]

    // console.log(item)
    await this.setState({
      tourDates: this.state.tourDates.concat(item2)
    })
    console.log(this.state.tourDates)

  }
  removeDate = async (tourDate) => {
    console.log(tourDate)
    await this.setState({
      tourDates: this.state.tourDates.filter(file => file !== tourDate)
    });
    console.log(this.state.tourDates)
  }


  //------------------------------ Date
  setTourDate = async (tourDateIndex, tourDate) => {
    let items = [...this.state.tourDates];
    let item = { ...items[tourDateIndex] };
    item = tourDate;
    items[tourDateIndex] = item;
    this.setState({ tourDates: items });

    let minDate = new Date()
    minDate.setHours(new Date().getHours() + 1)
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;
    fieldValidationErrors.tourDate = false
    fieldValidationCheck.tourDate = true
    await this.setState({
      tourSchedules: items,
      tourDate,
      formErrors: fieldValidationErrors,
      formCheck: fieldValidationCheck,
    }, this.validateForm)
    console.log(this.state.tourSchedules)
  }

  //-------------------------- other inputs
  handleUserInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value },
      () => { this.validateField(name, value) });
  }

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;
    switch (fieldName) {
      case 'title':
        let titleValid = value.length >= 1 && value.length < 50;
        fieldValidationErrors.title = titleValid ? false : true;
        fieldValidationCheck.title = titleValid ? true : false;
        break;
      case 'region':
        let regionValid = value.length >= 1 && value.length < 30;
        fieldValidationErrors.region = regionValid ? false : true;
        fieldValidationCheck.region = regionValid ? true : false;
        break;
      case 'description':
        let descriptionValid = value.length >= 50 && value.length < 5000;
        fieldValidationErrors.description = descriptionValid ? false : true;
        fieldValidationCheck.description = descriptionValid ? true : false;
        break;
      case 'price':
        let verifyNegativeNumber = Math.sign(value)
        let priceValid = value.length >= 1;
        fieldValidationErrors.price = priceValid ? false : true;
        fieldValidationCheck.price = priceValid ? true : false;
        if (verifyNegativeNumber === -1) {
          fieldValidationErrors.price = true
        }
        break;
      case 'maxParticipants':
        let verifyNegativeParticipants = Math.sign(value)
        let maxParticipantsValid = value.length >= 1;
        fieldValidationErrors.maxParticipants = maxParticipantsValid ? false : true;
        fieldValidationCheck.maxParticipants = maxParticipantsValid ? true : false;
        if (verifyNegativeParticipants === -1) {
          fieldValidationErrors.maxParticipants = true
        }
        break;
      case 'modality':
        let modalityValid = value.length !== '';
        fieldValidationErrors.modality = modalityValid ? false : true;
        fieldValidationCheck.modality = modalityValid ? true : false;
        break;
      case 'estimatedHour':
        let estimatedHourValid = value.includes('_') || value === "00:00" || value === "";
        fieldValidationErrors.estimatedHour = estimatedHourValid ? true : false;
        fieldValidationCheck.estimatedHour = estimatedHourValid ? true : false;
        break;
      case 'tourDate':
        let tourDateValid = value !== '';
        fieldValidationErrors.tourDate = tourDateValid ? false : true;
        fieldValidationCheck.tourDate = tourDateValid ? true : false;

        break;
      case 'imgIdMain':
        let imgIdMainValid = value !== '';
        fieldValidationErrors.imgIdMain = imgIdMainValid ? false : true;
        fieldValidationCheck.imgIdMain = imgIdMainValid ? true : false;
        break;
      default:
        break;
    }
    this.setState({
      formErrors: fieldValidationErrors,
      formCheck: fieldValidationCheck
    }, this.validateForm);
  }

  validateForm() {
    this.setState({
      formValid:
        this.state.formErrors.title === false &&
        this.state.formErrors.region === false &&
        this.state.formErrors.description === false &&
        this.state.formErrors.price === false &&
        this.state.formErrors.estimatedHour === false &&
        this.state.formErrors.tourDate === false &&
        this.state.formErrors.mainImg === false &&
        this.state.formErrors.maxParticipants === false &&
        this.state.formErrors.modality === false
    });
  }


  async handleError(error) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      const errorMessage = error.error.message
      return alert(errorMessage)
    } else {
      console.log(error)
      if (error === "notloggedin") {
        errorMessage = "User is not logged!"
        this.showErrMessage(errorMessage)
      } else if (error === "unabledataget") {
        errorMessage = "'Unable to retrieve Post Data'"
        this.showErrMessage(errorMessage)
      } else {
        if (error.response.status === 403) {
          // return alert("Email or password is incorrect")
          errorMessage = "User isn't the owner of the post"
          this.showErrMessage(errorMessage)
        } else if (error.response.status === 404) {
          errorMessage = "Rong data"
          this.showErrMessage(errorMessage)
        } else {
          //-----------just show the error
          this.showErrMessage(error)
        }
      }

    }
    this.setState({
      error: errorMessage,
      loading: false,
      // submittedPassword: false
    })
  }


  showErrMessage(errMessage) {
    store.addNotification({
      title: "Error!",
      message: errMessage,
      type: "danger",
      insert: "top",
      container: "top-right",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000,
        onScreen: true,
        pauseOnHover: true,
        showIcon: true

      }
    });
  }

  showSuccessMessage(successMessage) {
    store.addNotification({
      title: "Success!",
      message: successMessage,
      type: "success",
      insert: "top",
      container: "top-right",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000,
        onScreen: true,
        pauseOnHover: true,
        showIcon: true
      }
    });
  }

  //------------------------------------------------- Render ----------------------------------------------------------------
  render() {
    const { uploadedFiles, uploadMainFile } = this.state;

    return (
      <div className="backGroundRegisterPost" style={{ backgroundImage: "url(" + this.state.imageBackgrount + ")" }} >
        <Header style={{ position: 'sticky' }} />

        {this.state.canRedirectPosts && this.state.isBackToMyTours === false && (
          <Redirect to={{ pathname: '/tours' }} />
        )}
        {this.state.canRedirectPosts && this.state.isBackToMyTours === true && (
          <Redirect to={{ pathname: '/myTours' }} />
        )}
        <div className='cardPositionRegPost'>
          <Card className='handleCardPostReg' >
            <Card.Title className="coverTitleRegisterPost">Tour register</Card.Title>
            <div className='divAlingItems'>

              <div className='formPositionRegPost'>
                <InputGroup className='inputGroupPost'>
                  <Form.Control type='text' placeholder='Tour Title' id='title' name='title'
                    // onChange={this.updateFields} 
                    isInvalid={this.state.formErrors.title}
                    // isValid={this.state.formCheck.title}
                    value={this.state.title}
                    onChange={this.handleUserInput}
                  // onChange={e => this.setState({ title: e.target.value, submittedTitle: true })}
                  // value={this.state.title} 
                  />
                </InputGroup>
                {!this.state.formErrors.title === false && this.state.title.length <= 1 &&
                  <div className="help-blockRegPost">Title is required</div>
                }
                {!this.state.formErrors.title === false && this.state.title.length > 50 &&
                  <div className="help-blockRegPost">Title limit is 50 characters</div>
                }
              </div>
              <div className='formPositionRegPost'>
                <InputGroup className='inputGroupPost'>
                  <Form.Control type='text' placeholder='Region' id='region' name='region'
                    // onChange={this.updateFields} 
                    isInvalid={this.state.formErrors.region}
                    // isValid={this.state.formCheck.region}
                    value={this.state.region}
                    onChange={this.handleUserInput}
                  // onChange={e => this.setState({ region: e.target.value, submittedRegion: true })}
                  // value={this.state.region} 
                  />
                </InputGroup>
                {!this.state.formErrors.region === false && this.state.region.length <= 1 &&
                  <div className="help-blockRegPost">Region is required</div>
                }
                {!this.state.formErrors.region === false && this.state.region.length > 30 &&
                  <div className="help-blockRegPost">Region limit is 30 characters</div>
                }
              </div>
              <div className='formPositionRegPost'>
                <InputGroup className='inputGroupPost'>
                  <Form.Control as="textarea" type='text' placeholder='Tour description' id='description' name='description'
                    // onChange={this.updateFields} value={this.state.description} 
                    isInvalid={this.state.formErrors.description}
                    // isValid={this.state.formCheck.description}
                    value={this.state.description}
                    onChange={this.handleUserInput}
                  // onChange={e => this.setState({ description: e.target.value, submittedDescription: true })}
                  // value={this.state.description} 
                  />
                </InputGroup>
                {!this.state.formErrors.description === false && this.state.description.length < 1 &&
                  <div className="help-blockRegPost">Description is required</div>
                }
                {!this.state.formErrors.description === false && this.state.description.length < 50 && this.state.description.length > 0 &&
                  <div className="help-blockRegPost">Description must be at least 50 characters</div>
                }
                {!this.state.formErrors.description === false && this.state.description.length > 5000 &&
                  <div className="help-blockRegPost">Description limit is 5000 characters</div>
                }

              </div>
              <div className='formPositionRegPost'>
                <InputGroup className='inputGroupPost'>
                  <Form.Control type='number' placeholder='Tour Price' id='price' name='price'
                    isInvalid={this.state.formErrors.price}
                    // isValid={this.state.formCheck.price}
                    value={this.state.price}
                    onChange={this.handleUserInput}
                  // onChange={e => this.setState({ price: e.target.value, submittedPrice: true })}
                  // value={this.state.price} 
                  />
                </InputGroup>
                {!this.state.formErrors.price === false && this.state.price.length <= 1 &&
                  <div className="help-blockRegPost">Price is required</div>
                }
                {this.state.formErrors.price && this.state.price.includes('-') &&
                  <div className="help-blockRegPost">Price can't be negative</div>
                }
              </div>
              <div className='formPositionRegPost'>
                <InputGroup className='inputGroupPost'>
                  <Form.Control type='number' placeholder='Maximum of participants' id='maxParticipants' name='maxParticipants'
                    isInvalid={this.state.formErrors.maxParticipants}
                    value={this.state.maxParticipants}
                    onChange={this.handleUserInput}
                  />
                </InputGroup>
                {!this.state.formErrors.maxParticipants === false && this.state.maxParticipants.length <= 1 &&
                  <div className="help-blockRegPost">Maximum of participants is required</div>
                }
                {this.state.formErrors.maxParticipants && this.state.maxParticipants.includes('-') &&
                  <div className="help-blockRegPost">Maximum of participants can't be negative</div>
                }
              </div>

              <div className='formPositionRegPost'>
                <Form.Group >
                  {/* <Form.Label>State</Form.Label> */}
                  <Form.Control as="select" id='modality' name='modality'
                    isInvalid={this.state.formErrors.modality}
                    value={this.state.modality}
                    onChange={this.handleUserInput}
                  >
                    <option value='' >Modality...</option>
                    <option value='On Foot' >On Foot</option>
                    <option value='Bike'>Bike</option>
                    <option value='Boat'>Boat</option>
                    <option value='Auto'>Auto</option>
                    <option value='Trail'>Trail</option>
                    <option value='Horse'>Horse</option>
                    <option value='Other'>Other</option>
                  </Form.Control>
                </Form.Group>
                {this.state.formErrors.modality === false && this.state.modality === '' &&
                  <div className="help-blockRegPost">Modality is required</div>
                }
              </div>
              {/* ------------------------------------------ Timer ---------------------------------------------------------------- */}

              <div className='formPositionRegEstimatedHour' onClick={() => this.setState({ showCalendar: false })} >
                <Row >
                  <Form.Label className="labelTimePicker">Tourn estimated hour:</Form.Label>
                  <MaskedFormControl
                    className="fieldEstimated"
                    type='text'
                    id='estimatedHour'
                    name='estimatedHour'
                    mask='11:11'
                    placeholder='hh:mm'
                    value={this.state.estimatedHour}
                    onChange={(time) => this.handleUserInput(time)}
                  />
                </Row>
                {this.state.formErrors.estimatedHour === true &&
                  <div className="help-blockRegPost">Estimated hour is required</div>
                }
              </div>
              {/* ------------------------------------------ Dates Array ---------------------------------------------------------------- */}

              {this.state.tourDates.map((tourDate, index) => (
                <div key={index}>
                  <div className='formPositionRegPost'>
                    <Row className='rowTimePicker'>
                      <Form.Label className="labelTimePicker">Date of the tour:</Form.Label>
                      <div className="styleDatePostRegister">
                        <DatePicker
                          // onChange={e => this.setState({ submittedtourDate: true })}
                          selected={new Date(tourDate)}
                          // selected={new Date(this.state.tourSchedules[0].tourDate)}
                          onChange={date => this.setTourDate(index, date)}
                          // onSelect={e => this.setState({ submittedtourDate: true })} //when day is clicked
                          customInput={<CustomInputDate />}
                          // showTimeInput={true}
                          showTimeSelect
                          timeIntervals={15}
                          showMonthDropdown
                          showYearDropdown
                          dropdownMode="select"
                          minDate={(new Date())}
                          excludeDates={this.state.tourDates}
                          highlightDates={this.state.tourDates}
                          // minTime={new Date()}
                          timeFormat="HH:mm"
                          dateFormat="MMMM d, yyyy h:mm aa"
                        />


                      </div>
                      {/* <div> */}
                      {index > 0 &&
                        <div className="btnPositionPlusPostReg">
                          <Button className='btnIconAddRegPost' variant="link"
                            onClick={() => this.removeDate(tourDate)}
                          >
                            <FontAwesomeIcon
                              className='iconPlusPostReg'
                              icon={faMinusCircle}
                            />
                          </Button>
                        </div>
                      }
                      {this.state.tourDates.length - 1 === index && this.state.tourDates.length < 10 &&
                        <div className="btnPositionPlusPostReg">
                          <Button className='btnIconAddRegPost' variant="link"
                            onClick={e => this.creteNewDate(index)}
                          >
                            <FontAwesomeIcon
                              className='iconPlusPostReg'
                              icon={faPlusCircle}
                            />
                          </Button>
                        </div>
                      }
                      {/* </div> */}
                    </Row>
                    {!this.state.formErrors.tourDate === false && this.state.tourDate &&
                      <div className="help-blockRegPost">Tour must be at least 1 hour longer than current time</div>
                    }
                  </div>
                </div>
              ))}
              {/* ------------------------------------------------------------------------------------------------------------------------- */}
              <div className='alingIemsDateRegPost' >
                <ListGroup.Item className='datesItemRegPost' > </ListGroup.Item>
                <ListGroup.Item className='datesItemRegPostBotton'> </ListGroup.Item>
              </div>
              {/* <div className='formPositionRegPost'>
                  <Button className='btnIconAddRegPost' variant="link">
                    <FontAwesomeIcon
                      className='iconPlusPostReg'
                      icon={faPlusCircle}
                    />
                  </Button>


                </div> */}


              <div className='formPositionRegPost'>
                {/* <Form> */}
                <Row className='rowCustomSwitch'>
                  <div className='custom-control custom-switch'>
                    <Col>
                      <input
                        type='checkbox'
                        className='custom-control-input'
                        id='customSwitches'
                        readOnly
                        checked={this.state.transport}
                        onChange={this.handleSwitchChange('transport')}
                      />
                      <label className='custom-control-label' htmlFor='customSwitches'>
                        Include Transport
                        </label>
                    </Col>
                  </div>

                  <div className='custom-control custom-switch'>
                    <Col>
                      <input
                        type='checkbox'
                        className='custom-control-input'
                        id='customSwitchesChecked'
                        readOnly
                        checked={this.state.stay}
                        onChange={this.handleSwitchChange('stay')}
                      />
                      <label className='custom-control-label' htmlFor='customSwitchesChecked'>
                        Include stay
                      </label>
                    </Col>
                  </div>
                </Row>
              </div>
              {/* ---------------------------------------------IMAGES ---------------------------------------------- */}
              {/* ------------------------------------------ Principal image ----------------- */}
              <div className='imagesPosition'>
                <Row className='rowImagePosition'>
                  <div className="divMainImage">
                    <Col>
                      <Content>
                        {uploadMainFile.length < 1 && (
                          <PrincipalUpload onUpload={(files) => this.handleUpload(files, true)} />
                        )}
                        {!!uploadMainFile.length && (
                          <PrincipalFileList files={uploadMainFile} onDelete={(files, deleteLocal) => this.handleDelete(files, true, deleteLocal)} />
                        )}
                      </Content>
                    </Col>
                    {this.state.submittedMainImg && this.state.desableByMainImg === '' &&
                      <div className="help-blockRegPost">Main Image is required</div>
                    }
                  </div>
                  {/* --------------------------- Other images ----------------- */}
                  <div className="divOtherImages">
                    <Col>
                      <Content>
                        {uploadedFiles.length < 4 && (
                          <Upload onUpload={(files) => this.handleUpload(files, false)} />
                        )}
                        {!!uploadedFiles.length && (
                          <FileList files={uploadedFiles} onDelete={(files, deleteLocal) => this.handleDelete(files, false, deleteLocal)} />
                        )}
                      </Content>
                    </Col>
                  </div>
                </Row>

              </div>
              {/* -------------------------------------------------------------------------------------------------- */}
            </div>
            {/* <div className='formPositionRegPost'>
                <div style={{ margin: 'auto' }}>
                  <Button onClick={() => this.saveDataApi(false)}>Save post</Button>
                </div>
              </div> */}
            <div >
              <ListGroup.Item className='loginItem' > </ListGroup.Item>
              <ListGroup.Item className='loginItemBotton'> </ListGroup.Item>
            </div>
            <div className='buttonPositionSavepost'>
              <Button
                disabled={
                  !this.state.formValid || this.state.loading
                }
                onClick={!this.state.loading ? () => this.saveDataApi(false) : null}
                className='buttonSavePost'
              // style={{ backgroundColor: '#fa6980', borderColor: '#fa6980' }}
              >
                {this.state.loading ? 'Checking...' : 'Save post'}
              </Button>
            </div>
          </Card>
        </div>

      </div >
    )
  }

}
