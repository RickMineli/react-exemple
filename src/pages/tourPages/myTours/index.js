import React, { Component } from 'react'

import { Button, Form, Col, Row } from 'react-bootstrap'
// import Search from '../../components/search/search'
import Header from '../../../components/header/Header'
import Footer from '../../../components/footer/Footer'
import FeedSearchTour from '../../../components/FeedSearchTour/FeedSearchTour'
// import { isMobile } from 'react-device-detect';
import { storage } from '../../../services/firebase';
import { store } from 'react-notifications-component';

/* COLOCAR OS ICONES */
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faPlusCircle } from '@fortawesome/free-solid-svg-icons'

import './index.css'
import * as serviceTours from '../../../services/tours.service'
// import { Animated } from "react-animated-css";
// import loadingGif from '../../../imgs/loadingGifs/loading12.gif'

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

export default class index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisibleSearchHeader: true,

            myTours: [],
            originalItems: [],
            userId: '',



            locationInput: '',
            dateInput: new Date(),
            paginationNumber: 0,
            // trandingTours: [],
            // newTripsToursBig: [],
            // newTripsToursLittle: [],

            loading: false,
            // originalItems: [],
            // tours: [],

            data: {
                locationInput: '',
                dateInput: new Date()
            },
            //----------------- Validations
            formValid: false,
            formErrors: {
                locationInput: '',
                dateInput: ''
            },
            formCheck: {
                locationInput: '',
                dateInput: ''
            },
        };
    }


    async componentDidMount() {
        const dataUser = JSON.parse(localStorage.getItem('user'))
        serviceTours.getGuideTours(dataUser.userid)
            .then(res => {
                console.log(res.data)
                this.setState({
                    userId: dataUser.userid,
                    myTours: res.data,
                    originalItems: res.data
                })
            }, err => {
                console.log(err)
            })
    }

    filterList = async () => {
        let seacrchValue = this.state.locationInput
        var updatedList
        updatedList = this.state.originalItems.filter(function (item) {
            return item.region.toLowerCase().search(seacrchValue.toLowerCase()) !== -1;
        });
        this.setState({
            myTours: updatedList,
        });
    }

    handleSearchInput = (e) => {
        let newData = this.state.data;
        const name = e.target.name;
        const value = e.target.value;
        newData[name] = value
        this.setState({
            [name]: value,
            data: newData
        },
            () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let fieldValidationCheck = this.state.formCheck;
        switch (fieldName) {
            case 'locationInput':
                let locationInputValid = value.length >= 0;
                fieldValidationErrors.locationInput = locationInputValid ? false : true;
                fieldValidationCheck.locationInput = locationInputValid ? true : false;
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            formCheck: fieldValidationCheck
        }, this.validateForm);
    }

    validateForm() {
        this.setState({
            formValid:
                this.state.formErrors.locationInput === false ||
                this.state.formErrors.dateInput === false
        });
    }
    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            if (this.state.formValid === true) {
                this.filterList()
            }
        }
    }


    alertDelete = (tour) => {
        console.log(tour)
        confirmAlert({
            title: 'Are you sure?',
            message: 'You want to delete this Tour.',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.removeTour(tour)
                },
                {
                    label: 'No',
                    // onClick: () => alert('Click No')
                }
            ]
        });
    };

    removeTour = async (tour) => {
        try {
            await serviceTours.removeTours(tour.id)
                .then(res => {
                    console.log("Deleted tour with id: " + tour.id + " and Region:", tour.region)
                    this.handleDeleteImages(tour)
                }, err => {
                    console.log(err)
                })
            this.componentDidMount()
        } catch (error) {
            alert('Não foi possivel excluir o Produto!')

        }
    }

    handleDeleteImages = async (tour) => {
        console.log(tour)
        try {
            if (tour.mainImage.firebaseId !== "null") {
                this.deleteOneImage(tour.id, tour.mainImage.fireBaseId)
            }
            for (let index = 0; index < tour.images.length; index++) {
                let imgId = tour.images[index].fireBaseId
                if (imgId !== "null") {
                    console.log('testeeees', tour.images[index].fireBaseId)
                    this.deleteOneImage(tour.id, tour.images[index].fireBaseId)
                }
            }
        } catch (error) {
            alert('Não foi possivel deletar a imagem')
            console.log(error)
        }finally{
            this.showSuccessMessage("Your tour was deleted")
        }

    }


    deleteOneImage = async (tourId, imageId) => {
        const storageRef = storage.ref();
        var desertRef = storageRef.child('tours').child(JSON.stringify(tourId)).child(imageId);
        // Delete the file
        await desertRef.delete()
            .then(res => {
                console.log(res)
                console.log(imageId, 'deleted')
            }, err => {
                console.log(err)
            })
    }

    showSuccessMessage(successMessage) {
        store.addNotification({
            title: "Succes:",
            message: successMessage,
            type: "success",
            insert: "bottom",
            container: "top-right",
            animationIn: ['animated', 'fadeIn'],
            animationOut: ["animated", "fadeOut"],
            // width: '300px',
            dismiss: {
                duration: 5000,
                // onScreen: true,
                // pauseOnHover: true,
                // showIcon: true
            }
        });
    }
    render() {
        const {
            myTours,
            locationInput,
            // dateInput,
            // isVisibleSearchHeader,
        } = this.state;
        return (
            <div className='alingFeed'>
                <Header />
                <div>
                    <Col>
                        <Row>
                            <Form.Control type="text" placeholder="Location" className="mr-sm-2 searcMyTours"
                                id='locationInput' name='locationInput'
                                value={locationInput}
                                onChange={(event) => this.handleSearchInput(event)}
                                onKeyPress={(event) => this.handleKeyPress(event)} />
                            <Button variant="outline-info" className='btnSearchMyTours'
                                disabled={!this.state.formValid || this.state.loading}
                                onClick={() => this.filterList()}>
                                {this.state.loading ? 'Searching...' : 'Search'}
                            </Button>
                            <Button variant="outline-info" className='buttonPlusNewTour'
                                href={`/myTourRegister/`}
                            >
                                New Tour
                                {/* <FontAwesomeIcon
                                    className='iconPlusMyTours'
                                    icon={faPlusCircle}
                                /> */}
                            </Button>
                        </Row>

                    </Col>

                </div>

                <div>
                    <div>
                        <FeedSearchTour myTours={myTours} removeTour={(pageIndex) => this.alertDelete(pageIndex)} />
                    </div>
                    {/* )} */}
                </div>
                <Footer />

            </div>

        )
    }
}
