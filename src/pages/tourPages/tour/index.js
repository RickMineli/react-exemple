import React, { Component } from 'react'

import './index.css'
import Cover from '../../../components/tourComponents/cover/Cover'
import Contents from '../../../components/tourComponents/contents/Contents'
import Description from '../../../components/tourComponents/Description/Description'
import { Row, Col } from 'react-bootstrap'
import Header from '../../../components/header/Header'
import Footer from '../../../components/footer/Footer'
import * as serviceTours from '../../../services/tours.service'
import { store } from 'react-notifications-component';
import BookingConfirmation from '../../../components/BookingConfirmation/BookingConfirmation';
import CreditCards from '../../../components/creditCard/CreditCards'

export default class index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // imageBackgrount: tmpImg,
      loading: false,
      imagesLink: [],

      //----- Data of API
      id: '',
      guide: [],
      tourDates: [new Date()],
      title: '',
      description: '',
      price: '',
      stay: false,
      transport: false,
      maxParticipants: '',
      creationDate: '',
      lastEditedDate: '',
      realCurrentTime: new Date(),
      // -- imgs
      mainImage: {},
      images: [],
      showBooking: false,
      payment: false,



      //--------------- Contract Tour
      estimatedHour: '00:00',
      region: '',
      dateChosen: '',
      totalPrice: '',
      participants: [
        {
          name: '',
          age: '',
          gender: '',
          documentName: '',
          document: ''
        }
      ],


      //----------------- Validations
      formValid: false,
      formErrors: [{
        name: '',
        age: '',
        gender: '',
        documentName: '',
        document: '',
      }],
      formCheck: [{
        name: '',
        age: '',
        gender: '',
        documentName: '',
        document: '',
      }]
    }
  }

  async componentDidMount() {
    try {
      if (this.props.match.params.id) {
        serviceTours.getTour(this.props.match.params.id)
          .then(res => {
            let data = res.data
            // let convertToDate = new Date().getTime(res.headers.currenttime)
            let day = new Date().getUTCDate(res.headers.currenttime)
            let month = new Date().getUTCMonth(res.headers.currenttime)
            let year = new Date().getUTCFullYear(res.headers.currenttime)
            let hours = new Date().getUTCHours(res.headers.currenttime) - 3
            let minute = new Date().getUTCMinutes(res.headers.currenttime)
            let seconds = new Date().getUTCSeconds(res.headers.currenttime)

            // console.log(new Date(res.headers.currenttime))
            // console.log(convertToDate)
            //----------------------------------To order images by original id
            var imgOrderById = [...data.images];
            imgOrderById.sort((a, b) => a.id - b.id);
            imgOrderById.map((item, i) => (<div key={i}> {item.id}
              {item.link} {item.fireBaseId}</div>))

            this.setState({
              realCurrentTime: new Date(year, month, day, hours, minute, seconds),
              loading: false,
              id: data.id,
              guide: data.guide,
              title: data.title,
              region: data.region,
              description: data.description,
              price: data.price,
              maxParticipants: data.maxParticipants,
              stay: data.stay,
              transport: data.transport,
              tourDates: data.tourDates,
              estimatedHour: data.estimatedHour,
              creationDate: data.creationDate,
              lastEditedDate: data.lastEditedDate,
              guidePicture: data.guide.picture.link,
              //--------------------------- imgs
              mainImage: data.mainImage,
              images: imgOrderById,//data.images,

              imageBackgrount: data.mainImage.link
            })



            for (let index = 0; index < this.state.images.length; index++) {
              if (this.state.images[index].fireBaseId !== 'null') {
                this.setState({
                  imagesLink: this.state.imagesLink.concat(this.state.images[index].link)
                });
              }
            }

            this.setState({
              imagesLink: this.state.imagesLink.concat(this.state.mainImage.link)
            });
            // console.log(this.state)
            if (data.mainImage.link === "null") {
              this.setState({
                // imageBackgrount: tmpImg,
              })
            }
          }, err => {
            this.handleError('unabledataget')
            console.log(err)
          })
      } else {
        this.creteNewDate()
      }
    } catch (error) {
      this.handleError('unabledataget')
      console.log(error)
    }
  }

  handleParticipantInput = (e, index) => {
    const name = e.target.name;
    const value = e.target.value;
    // this.setState({ [name]: value },
    //   () => { 
    this.validateField(name, value, index)
    // });
  }

  validateField(fieldName, value, index) {
    let fieldValidationErrors = this.state.formErrors;
    let fieldValidationCheck = this.state.formCheck;
    let items = [...this.state.participants]
    let item = { ...items[index] };
    switch (fieldName) {
      case 'name':
        item.name = value
        let nameValid = value.length >= 7 && value.length <= 50;
        fieldValidationErrors[index].name = nameValid ? false : true;
        fieldValidationCheck[index].name = nameValid ? true : false;
        break;
      case 'age':
        item.age = value
        let ageValid = value.length > 0 && value.length <= 3;
        fieldValidationErrors[index].age = ageValid ? false : true;
        fieldValidationCheck[index].age = ageValid ? true : false;
        break;
      case 'gender':
        item.gender = value
        let genderValid = value !== '' && value.length >= 4 && value.length <= 10;
        fieldValidationErrors[index].gender = genderValid ? false : true;
        fieldValidationCheck[index].gender = genderValid ? true : false;
        break;
      case 'documentName':
        item.documentName = value
        let documentNameValid = value.length >= 2 && value.length <= 15;
        fieldValidationErrors[index].documentName = documentNameValid ? false : true;
        fieldValidationCheck[index].documentName = documentNameValid ? true : false;
        break;
      case 'document':
        item.document = value
        let documentValid = value.length >= 6 && value.length <= 40;
        fieldValidationErrors[index].document = documentValid ? false : true;
        fieldValidationCheck[index].document = documentValid ? true : false;
        break;
      default:
        break;
    }
    items[index] = item;
    this.setState({
      participants: items,
      formErrors: fieldValidationErrors,
      formCheck: fieldValidationCheck
    }, () => this.validateForm(index));
  }

  validateForm(index) {
    this.setState({
      formValid:
        this.state.formErrors[index].name === false &&
        this.state.formErrors[index].age === false &&
        this.state.formErrors[index].gender === false &&
        this.state.formErrors[index].documentName === false &&
        this.state.formErrors[index].document === false
    });
  }



  creteNewParticipant = (e, index) => {

    const participant = [
      {
        name: '',
        age: '',
        gender: '',
        documentName: '',
        document: ''
      }
    ]
    const formErrors = [{
      name: '',
      age: '',
      gender: '',
      documentName: '',
      document: '',
    }]
    const formCheck = [{
      name: '',
      age: '',
      gender: '',
      documentName: '',
      document: '',
    }]

    this.setState({
      participants: this.state.participants.concat(participant),
      formErrors: this.state.formErrors.concat(formErrors),
      formCheck: this.state.formCheck.concat(formCheck)
    }, () => this.validateForm(index + 1))
  }

  removeParticipant = (participant, index) => {
    console.log(index)
    this.setState({
      participants: this.state.participants.filter(file => file !== participant),
      formErrors: this.state.formErrors.filter(file => file !== index),
      formCheck: this.state.formCheck.filter(file => file !== index)
    }, () => this.validateForm(index - 1));
  }

  goPayment = () => {
    this.setState({
      payment: true
    })
    console.log("go payment", this.state.participants)
  }

  handleSchedule = (checkDate) => {
    console.log("schedule Clicked", this.state.dateChosen)
    // this.showSuccessMessage('You tour has added')
    this.setState({
      showBooking: true
    })
  }

  setDate = (tourDate) => {
    console.log(tourDate)
  }

  tryPayContract = async () => {
    this.setState({
      loading: true
    })
    console.log('try pay', this.state)
    serviceTours.newTourContract(this.state)
      .then(res => {
        console.log("Success", res)

        const { history } = this.props;
        if (history) history.push('/');
        this.setState({
          loading: false
        })
      }, err => {
        console.log(err)
        this.setState({
          loading: false
        })
      })

  }

  showSuccessMessage(successMessage) {
    store.addNotification({
      title: "Success!",
      message: successMessage,
      type: "success",
      insert: "bottom",
      container: "top-right",
      animationIn: ['animated', 'fadeIn'],
      animationOut: ["animated", "fadeOut"],
      // width: '300px',
      dismiss: {
        duration: 5000,
        // onScreen: true,
        // pauseOnHover: true,
        // showIcon: true
      }
    });
  }
  render() {
    const { mainImage, images, imagesLink, participants, formValid, formErrors, formCheck } = this.state;
    return (
      <div className='alinhamentoPost'>
        <Header />
        <Cover
          mainImage={mainImage}
          imagesLink={imagesLink}
          title={this.state.title}
          onSchedule={() => this.handleSchedule()} />
        <div style={{ marginTop: '20px' }}>

          <div className='colPositionPost'>
            <Row>
              <div className='contentDivTour'>
                <Col>
                  <Contents
                    dateChosen={(tourDate) => this.setState({ dateChosen: tourDate })}
                    showBooking={this.state.showBooking}
                    guide={this.state.guide}
                    guidePicture={this.state.guidePicture}
                    allData={this.state}
                    onSchedule={() => this.handleSchedule()} />
                </Col>
              </div>
              <div className='descriptionDivTour'>
                <Col>
                  {this.state.showBooking === false &&
                    <div className='descriptionDivTourAling'>
                      <Description description={this.state.description} otherImages={images} imagesLink={imagesLink} region={this.state.region} />
                    </div>
                  }
                  {this.state.showBooking === true &&
                    <div className='bookingDivAling'>
                      <BookingConfirmation
                        loading={this.state.loading}
                        creteNewParticipant={this.creteNewParticipant}
                        removeParticipant={this.removeParticipant}
                        participants={participants}
                        handlerParticipantInput={this.handleParticipantInput}
                        formValid={formValid}
                        formErrors={formErrors}
                        formCheck={formCheck}
                        price={this.state.price}
                        goPayment={this.goPayment}
                        payment={this.state.payment}
                      />
                    </div>
                  }
                  {this.state.payment === true &&
                    <div className='bookingDivAling'>
                      <CreditCards
                        loading={this.state.loading}
                        payButton={this.tryPayContract}
                        participants={participants}
                        price={this.state.price}
                        formValid={formValid}
                      />
                    </div>
                  }
                </Col>
              </div>
            </Row>
          </div>

        </div>
        <Footer />
      </div >
    )
  }
}
