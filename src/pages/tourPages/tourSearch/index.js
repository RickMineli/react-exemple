import React, { Component } from 'react'
import { Row, Form, Col, Container, Card, InputGroup, ListGroup, Button } from 'react-bootstrap';
import Header from '../../../components/header/Header'
import Footer from '../../../components/footer/Footer'
import Zoom from 'react-reveal/Zoom'

import './index.css'
//--------------------------- To date
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-daterange-picker/dist/css/react-calendar.css' // For some basic styling. (OPTIONAL)
import CustomInputDate from '../../../components/dateComponent'

/* COLOCAR OS ICONES */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt, faCalendarDay } from '@fortawesome/free-solid-svg-icons'

import * as serviceTours from '../../../services/tours.service'

export default class index extends Component {
    state = {
        location: '',
        date: new Date(),

        loading: true,
        originalItems: [],
        posts: [],

        //----------------- Validations
        formValid: false,
        formErrors: {
            location: '',
            date: ''
        },
        formCheck: {
            location: '',
            date: ''
        },
    }

    filterList = async (event) => {
        let seacrchValue = this.state.location
        var updatedList
        updatedList = this.state.originalItems.filter(function (item) {
            return item.title.toLowerCase().search(seacrchValue.toLowerCase()) !== -1;
        });
        this.setState({
            posts: updatedList,
            location: seacrchValue
        });
    }

    async componentDidMount() {
        // localStorage.removeItem('searchItem')
        let localSearch = JSON.parse(localStorage.getItem('searchItem'))
        serviceTours.getAllTours()
            .then(async res => {
                console.log(res.data)
                console.log("localstorage", localSearch)
                //----------------------------------To order images by original id
                var postsOrderById = [...res.data];
                postsOrderById.sort((a, b) => a.id - b.id);
                postsOrderById.map((item, i) => (<div key={i}> {item.id}</div>))
                try {
                    this.setState({
                        loading: false,
                        originalItems: postsOrderById,
                        posts: postsOrderById,
                    })
                    if (localSearch) {
                        // let datasSearch = JSON.parse(this.props.match.params.id)
                        console.log(localSearch)
                        this.setState({
                            location: localSearch.location,
                            date: new Date(localSearch.date)
                        })
                        await this.filterList()
                        localStorage.removeItem('searchItem')
                    }
                } catch (error) {
                    this.setState({
                        loading: false
                    })
                    alert('Não foi possivel carregar a lista de Anuncios!')
                    console.log(error)
                }
            }, err => {
                this.setState({
                    loading: false
                })
                alert('Não foi possivel carregar a lista de Anuncios!', err)
                console.log(err)
            })


    }

    handleCalendarChange = async date => {
        let fieldValidationErrors = this.state.formErrors;
        let fieldValidationCheck = this.state.formCheck;
        fieldValidationErrors.date = false
        fieldValidationCheck.date = true
        this.setState({
            date,
            formErrors: fieldValidationErrors,
            formCheck: fieldValidationCheck,
        }, this.validateForm)
    }

    handleSearchInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value,
        },
            () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let fieldValidationCheck = this.state.formCheck;
        switch (fieldName) {
            case 'location':
                let locationValid = value.length >= 1;
                fieldValidationErrors.location = locationValid ? false : true;
                fieldValidationCheck.location = locationValid ? true : false;
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            formCheck: fieldValidationCheck
        }, this.validateForm);
    }

    validateForm() {
        console.log(this.state.formValidx)
        this.setState({
            formValid:
                this.state.formErrors.location === false ||
                this.state.formErrors.date === false
        });
    }
    handleKeyPress = (event) => {
        // const { router } = this.props;
        if (event.key === 'Enter') {
            // if (this.state.formValid === true) {
            this.filterList()
            // }
        }
    }


    render() {
        return (
            <div >
                <Header />
                <Zoom>
                    <div >
                        {this.state.loading && (
                            <p>Loading Tours...</p>
                        )}
                        {!this.state.loading && (
                            <div>
                                <Row className='rowPositionPostSearchInputs'>
                                    {/* <div> */}
                                    <InputGroup placeholder='Search...' className='locateFormPostSearch'>
                                        <InputGroup.Prepend>
                                            <InputGroup.Text id='inputGroupPrepend'><FontAwesomeIcon className='iconColorPostSearch' icon={faMapMarkerAlt} /></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <Form.Control
                                            id='location'
                                            name='location'
                                            value={this.state.location}
                                            type='text'
                                            placeholder='Location...'
                                            onChange={this.handleSearchInput}
                                            onKeyPress={this.handleKeyPress}
                                        />
                                    </InputGroup>
                                    {/* </div> */}
                                    <div className='formPositionDatePostSearch'>
                                        <Col>
                                            <InputGroup.Text className='inputGorupDatePostSearch'>
                                                <div className="divIconDatePostSearch">
                                                    <FontAwesomeIcon
                                                        className='iconSearchCalendarColor'
                                                        icon={faCalendarDay}
                                                    />
                                                </div>
                                                <DatePicker
                                                    value={this.state.date}
                                                    selected={this.state.date}
                                                    onSelect={this.handleCalendarChange} //when day is clicked
                                                    customInput={<CustomInputDate />}
                                                    // showTimeInput={true}
                                                    showMonthDropdown
                                                    showYearDropdown
                                                    dateFormat="MMMM d, yyyy"
                                                    withPortal
                                                // maxDate={(new Date())}
                                                />
                                            </InputGroup.Text>

                                        </Col>
                                    </div>
                                    <div className='buttonPositionPostSearch'>
                                        <Col  >
                                            <Button className='searchButtonPostSearch'
                                                // disabled={!this.state.formValid}
                                                onClick={this.filterList}
                                            >Search</Button>
                                        </Col>
                                    </div>
                                </Row>
                                {this.state.posts.map((post) => (
                                    <div key={post.id}>
                                        <ListGroup.Item action href={`/tour/${post.id}`}>
                                            <Container>
                                                <Row >
                                                    <Card.Img variant="top" src={post.mainImage.link} style={{ height: "180px", width: '300px', borderRadius: '6px' }} />
                                                    <Col>
                                                        <Card.Body>
                                                            <Card.Title>{post.title}</Card.Title>
                                                            <Card.Subtitle>
                                                                {post.description}
                                                            </Card.Subtitle>
                                                        </Card.Body>

                                                    </Col>
                                                </Row>
                                            </Container>
                                        </ListGroup.Item>
                                    </div>
                                ))}
                            </div>
                        )}
                    </div>
                    <Footer />
                </Zoom>
            </div>
        )
    }
}
