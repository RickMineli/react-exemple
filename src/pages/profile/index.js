import React, { Component } from 'react'
import './index.css'
import Fade from 'react-reveal/Fade'
import Profile from '../../components/profile/profile'
import Footer from '../../components/footer/Footer'
import Header from '../../components/header/Header'

export default class index extends Component {
  render () {
    const { userId = this.props.match.params.id } = this.props
    return (
      <div>
        <Header />
        <Fade>
          <Profile userId={userId} />
        </Fade>
        <Footer />
      </div>
    )
  }
}
