import React, { Component } from 'react'

import Header from '../../components/header/Header'
import Footer from '../../components/footer/Footer'
import RegisterUser from '../../components/registerUser/registerUser'

export default class index extends Component {
  render () {
    return (
      <div>
        <Header />
        <RegisterUser becomeGuide={true} />
        <Footer />
      </div>
    )
  }
}
