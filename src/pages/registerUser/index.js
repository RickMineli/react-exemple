import React, { Component } from 'react'
import './index.css'
import Header from '../../components/header/Header'
import Zoom from 'react-reveal/Zoom'
import RegisterUser from '../../components/registerUser/registerUser'
import Footer from '../../components/footer/Footer'

export default class Index extends Component {
  render() {
    return (
      <div>
        <Header showSignInUP={false} />
        <Zoom>
          <RegisterUser />
        </Zoom>
      <Footer />
      </div>
    )
  }
}
